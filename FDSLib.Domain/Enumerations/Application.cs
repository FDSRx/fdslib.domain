﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FDSLib.Common.Enumerations;

namespace FDSLib.Domain.Enumerations
{
    public enum Application
    {
        /// <summary>
        /// External store management tool (a.k.a. myPharmacyEngage).
        /// </summary>
        [EnumCode("ENG")]
        myPharmacyEngage = 1,

        /// <summary>
        /// The application is unknown.
        /// </summary>
        [EnumCode("UNKNOWN")]
        Unknown = 5
    }
}