﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Common.Enumerations;

namespace FDSLib.Domain.Enumerations
{
    public enum ServiceName
    {
        /// <summary>
        /// General pharmacy services.  The ability to receive medications from a pharmacy.
        /// </summary>
        [EnumCode("PHRM")]
        Pharmacy,

        /// <summary>
        /// Enhanced pharmacy services.  The ability to refill medications online, receive alerts, etc.
        /// </summary>
        [EnumCode("MPC")]
        MyPharmacyConnect,

        /// <summary>
        /// Rewards/Loyalty program.
        /// </summary>
        [EnumCode("RWRD")]
        Rewards
    }
}
