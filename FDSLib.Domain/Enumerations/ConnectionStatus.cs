﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Common.Enumerations;


namespace FDSLib.Domain.Enumerations
{
    public enum ConnectionStatus
    {
        [EnumCode("SYSENR")]
        [EnumName("System Enrolled")]
        [IsActive(true)]
        SystemEnrolled,

        [EnumCode("SLFENR")]
        [EnumName("Self Enrolled")]
        [IsActive(true)]
        SelfEnrolled,

        [EnumCode("SLFTRM")]
        [EnumName("System Terninated")]
        [IsActive(false)]
        SelfTerminated,

        [EnumCode("SYSTRM")]
        [EnumName("System Terminated")]
        [IsActive(false)]
        SystemTerminated,

        [EnumCode("NOTENR")]
        [EnumName("Not Enrolled")]
        [IsActive(false)]
        NotEnrolled,

        [EnumCode("UNKNOWN")]
        [EnumName("Unknown")]
        [IsActive(false)]
        Unknown
    }

    /// <summary>
    /// Translates various strings into an MpcConnectionStatus object.
    /// </summary>
    public static class ConnectionStatusTranslator
    {
        public static ConnectionStatus FromCode(string code)
        {
            try
            {
                ConnectionStatus status = Enum.GetValues(typeof(ConnectionStatus)).Cast<ConnectionStatus>().FirstOrDefault(x => x.GetCode().ToLower() == code.ToLower());

                return status;
            }
            catch
            {
                return ConnectionStatus.Unknown;
            }
        }
    }
}
