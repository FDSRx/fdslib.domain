﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Common.Enumerations;

namespace FDSLib.Domain.Enumerations
{
    public enum BusinessKeyTypeName
    {
        [EnumCode("NABP")]
        Nabp
    }
}
