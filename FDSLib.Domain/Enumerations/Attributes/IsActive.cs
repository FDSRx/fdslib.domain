﻿using System;
using System.Reflection;


namespace FDSLib.Domain.Enumerations
{
    /// <summary>
    /// Enum attribute that allows a friendly name representation of the Enum.
    /// </summary>
    public class IsActive : System.Attribute
    {
        private readonly bool _value;

        public IsActive(bool value)
        {
            _value = value;
        }

        public bool Value
        {
            get { return _value; }
        }

    }

    /// <summary>
    /// Enum name extension methods.
    /// </summary>
    public static class EnumConnectionStatusExtensions
    {
        /// <summary>
        /// Indicates whether the connection status is considered active or inactive.
        /// </summary>
        /// <param name="value">The enumeration object.</param>
        /// <returns></returns>
        public static bool IsActive(this ConnectionStatus value)
        {
            bool output = false;

            try
            {
                Type type = value.GetType();

                FieldInfo fi = type.GetField(value.ToString());
                IsActive[] attrs = fi.GetCustomAttributes(typeof(IsActive), false) as IsActive[];

                if (attrs != null && attrs.Length > 0)
                {
                    output = attrs[0].Value;
                }
            }
            catch { }

            return output;
        }
    }


}
