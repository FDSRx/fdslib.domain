﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FDSLib.Common.Enumerations;

namespace FDSLib.Domain.Enumerations
{
    public enum AccessFilterType
    {
        /// <summary>
        /// Client's IP Address
        /// </summary>
        [EnumCode("IPADDR")]
        IpAddress = 1,

        /// <summary>
        /// Client's machine name.
        /// </summary>
        [EnumCode("MCHNME")]
        MachineName = 2,

        /// <summary>
        /// Domain name.
        /// </summary>
        [EnumCode("DOMNME")]
        DomainName = 3
    }
}