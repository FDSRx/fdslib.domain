﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FDSLib.Common.Enumerations;

namespace FDSLib.Domain.Enumerations
{
    public enum AccessDirective
    {
        /// <summary>
        /// Allows access.
        /// </summary>
        [EnumCode("A")]
        Allow = 1,

        /// <summary>
        /// Denies access.
        /// </summary>
        [EnumCode("D")]
        Deny = 2
    }
}