﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Common.Enumerations;

namespace FDSLib.Domain.Enumerations
{
    public enum CultureName
    {
        [EnumCode("en")]
        English,

        [EnumCode("es")]
        Spanish,

        [EnumCode("fr")]
        French
    }
}
