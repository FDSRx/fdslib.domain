﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FDSLib.Domain.Enumerations
{
    /// <summary>
    /// Defines a list of strongly-typed keys that can be used when storing and retrieving items from memory (e.g. session, cache, etc.).
    /// </summary>
    public static class CacheKey 
    {
        /// <summary>
        /// Unique key appended to session name so that the session namespace is not poluted.
        /// </summary>
        private const string UNIQUE_KEY = "EDC52A28A6CBC9FC5D68DB5A926C9";

        /// <summary>
        /// Returns a unique cache key.
        /// </summary>
        /// <param name="name">Name of key.</param>
        /// <remarks>Helps avoid cache/session collision.</remarks>
        /// <returns></returns>
        public static string Create(string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            return name + "-" + UNIQUE_KEY;
        }

        #region Cache Keys

        public static string Login { get { return Create("Login"); } }
        public static string StoreToken { get { return Create("StoreToken"); } }
        public static string ChainToken { get { return Create("ChainToken"); } }
        public static string ApplicationConfiguration { get { return Create("ApplicationConfiguration");  } }
        public static string Drug { get { return Create("Drug"); } }
        public static string MemberViews { get { return Create("MemberViews"); } }
        public static string IsRegistrationAuthorized { get { return Create("IsRegistrationAuthorized"); } }
        public static string IsRegistrationSessionTimedOut { get { return Create("IsRegistrationSessionTimedOut"); } }
        public static string IsAccountLocked { get { return Create("IsAccountLocked"); } }
        public static string IsAccountDeactivated { get { return Create("IsAccountDeactivated"); } }
        public static string IsAccountAuthorized { get { return Create("IsAccountAuthorized"); } }
        public static string AuthorizationToken { get { return Create("AuthorizationToken"); } }
        public static string IsAuthorizationTokenExpired { get { return Create("IsAuthorizationTokenExpired"); } }
        public static string TemporaryAccountToken { get { return Create("TemporaryAccountToken"); } }
        public static string TemporaryRegistrationToken { get { return Create("TemporaryRegistrationToken"); } }
        public static string TemporaryAccountChangePasswordToken { get { return Create("TemporaryAccountChangePasswordToken"); } }
        public static string ImageRepository { get { return Create("ImageRepository"); } }

        /// <summary>
        /// Cache of a store object.
        /// </summary>
        public static string Store
        {
            get { return Create("Store"); }
        }

        /// <summary>
        /// Cache of a pharmacy object (a variation of the store object).
        /// </summary>
        public static string Pharmacy
        {
            get { return Create("Pharmacy"); }
        }

        /// <summary>
        /// Cache of a User object.
        /// </summary>
        public static string User
        {
            get { return Create("User"); }
        }

        /// <summary>
        /// Cache of an enumerable list of store banners.
        /// </summary>
        public static string StoreBannerList
        {
            get { return Create("StoreBanner"); }
        }

        /// <summary>
        /// Cache of an enumerable list of opportunity categories.
        /// </summary>
        public static string OpportunityCategoryList
        {
            get { return Create("OpportunityCategoryList"); }
        }

        /// <summary>
        /// Cache of an enumerable list of opportunity types.
        /// </summary>
        public static string OpportunityTypeList
        {
            get { return Create("OpportunityTypeList"); }
        }

        /// <summary>
        /// Cache of an enumerable list of note types.
        /// </summary>
        public static string NoteTypeList
        {
            get { return Create("NoteTypeList"); }
        }

        /// <summary>
        /// Cache of an enumerable list of priorities.
        /// </summary>
        public static string PriorityList
        {
            get { return Create("PriorityList"); }
        }

        /// <summary>
        /// Cache of an enumerable list of scopes.
        /// </summary>
        public static string ScopeList
        {
            get { return Create("ScopeList"); }
        }

        /// <summary>
        /// Cache of an enumerable list of notebooks.
        /// </summary>
        public static string NotebookList
        {
            get { return Create("NotebookList"); }
        }

        /// <summary>
        /// Cache of an enumerable list of interaction types.
        /// </summary>
        public static string InteractionTypeList
        {
            get { return Create("InteractionTypeList"); }
        }

        /// <summary>
        /// Cache of an enumerable list of disposition types.
        /// </summary>
        public static string DispositionTypeList
        {
            get { return Create("DispositionTypeList"); }
        }

        /// <summary>
        /// Cache of an enumerable list of contact types.
        /// </summary>
        public static string ContactTypeList
        {
            get { return Create("ContactTypeList"); }
        }

        /// <summary>
        /// Cache of an enumerable list of medication types.
        /// </summary>
        public static string MedicationTypeList
        {
            get { return Create("MedicationTypeList"); }
        }

        /// <summary>
        /// Cache of an enumerable list of medication classification.
        /// </summary>
        public static string MedicationClassificationList
        {
            get { return Create("MedicationClassificationList"); }
        }

        /// <summary>
        /// Cache of an enumerable list of prescriber types.
        /// </summary>
        public static string PrescriberTypeList
        {
            get { return Create("PrescriberTypeList"); }
        }

        /// <summary>
        /// The cached MPC patient unique identifier.
        /// </summary>
        public static string MpcPatientKey
        {
            get { return Create("MpcPatientKey"); }
        }

        /// <summary>
        /// A cached collcetion of various patient authentication tokens.
        /// </summary>
        public static string PatientAuthenticationTokenCollection
        {
            get { return Create("PatientAutheticationTokenCollection"); }
        }

        /// <summary>
        /// Cache of a Patient object.
        /// </summary>
        public static string Patient
        {
            get { return Create("Patient"); }
        }

        /// <summary>
        /// Cache of an enumerable list of CVX codes.
        /// </summary>
        public static string CVXCodeList
        {
            get { return Create("CVXCodeList"); }
        }

        /// <summary>
        /// Cache of an enumerable list of administration route codes.
        /// </summary>
        public static string AdministrationRouteList
        {
            get { return Create("AdministrationRouteList"); }
        }

        /// <summary>
        /// Cache of an enumerable list of administration site codes.
        /// </summary>
        public static string AdministrationSiteList
        {
            get { return Create("AdministrationSiteList"); }
        }

        /// <summary>
        /// Cache of an enumerable list of MVX codes.
        /// </summary>
        public static string MVXCodes
        {
            get { return Create("MVXCodes"); }
        }

        /// <summary>
        /// Cache of an enumerable list of financial classes.
        /// </summary>
        public static string FinancialClasses
        {
            get { return Create("FinancialClasses"); }
        }

        /// <summary>
        /// Cache of an enumerable list of PredefinedMessage objects.
        /// </summary>
        public static string PredefinedMessageList
        {
            get { return Create("PredefinedMessageList"); }
        }

        /// <summary>
        /// Cache of an enumerable list of pharmacy plan objects.
        /// </summary>
        public static string PharmacyPlanList
        {
            get { return Create("PharmacyPlanList"); }
        }

        /// <summary>
        /// Cache of an enumerable list of adjudication plan objects.
        /// </summary>
        public static string AdjudicationPlanList
        {
            get { return Create("AdjudicationPlanList"); }
        }

        /// <summary>
        /// Cache of the five star score card.
        /// </summary>
        public static string FiveStarBanner
        {
            get { return Create("FiveStarBanner"); }
        }

        #endregion

    }





}