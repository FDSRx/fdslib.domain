﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Common.Enumerations;

namespace FDSLib.Domain.Enumerations
{
    /// <summary>
    /// A list of languages supported by the system.
    /// </summary>
    public enum LanguageType
    {
        /// <summary>
        /// The native system langauge.
        /// </summary>
        [EnumCode("en")]
        English,

        /// <summary>
        /// Spanish.
        /// </summary>
        [EnumCode("es")]
        Spanish
    }
}
