﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FDSLib.Common.Enumerations;

namespace FDSLib.Domain.Enumerations
{
    public enum AccessInspectionType
    {
        [EnumCode("HOST")]
        Host = 1,

        [EnumCode("ENVVAR")]
        EnvironmentVariable = 2
    }
}