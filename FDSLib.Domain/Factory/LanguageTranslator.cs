﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Domain.Enumerations;

namespace FDSLib.Domain.Factory
{
    public static class LanguageTranslator
    {
        /// <summary>
        /// Translates the language code or common text into an Enumerations.LanguageType object.
        /// </summary>
        /// <param name="language">The language code or common text (e.g. "en" or "english" could be translated to LanguageType.English).</param>
        /// <returns></returns>
        public static LanguageType GetLangaugeType(string language)
        {
            LanguageType type;

            switch (language.ToLower())
            {
                case "en":
                case "english":
                    type = LanguageType.English;
                    break;
                case "es":
                case "spanish":
                    type = LanguageType.Spanish;
                    break;
                default:
                    type = LanguageType.English;
                    break;
            }

            return type;
        }



    }




}
