using System;

namespace FDSLib.Domain.Interfaces
{
    public interface IMonitorService : IDisposable
    {
        event EventHandler OnConnected;
        event EventHandler OnConnectionClosed;
        event EventHandler<UnhandledExceptionEventArgs> OnError;

        string ServerEndpoint { get; }
        void StartService();
        void RegisterEndpoint<T>(string endpoint, Action<T> completeCallback);
        void InvokeEndpoint(string endpoint, params object[] arg);
    }
}