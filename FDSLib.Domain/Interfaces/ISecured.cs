using System.Security;

namespace FDSLib.Domain.Interfaces
{
    public interface ISecured
    {
        SecureString Password { get; }
    }
}