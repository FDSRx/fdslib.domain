﻿
namespace FDSLib.Domain.Interfaces
{
    public interface IUserService
    {
        bool ValidateUser(string userName, string ncpdpId, string password);
    }
}
