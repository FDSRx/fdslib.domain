using System;
using System.Runtime.InteropServices;
using System.Security;

namespace FDSLib.Domain.Extensions
{
    public static class SecurityExtensions
    {
        public static string Unsecure(this SecureString securePassword)
        {
            if (securePassword == null)
            {
                throw new ArgumentException("securePassword");
            }

            IntPtr passwordPtr = IntPtr.Zero;

            try
            {
                var ptr = Marshal.SecureStringToBSTR(securePassword);
                return Marshal.PtrToStringBSTR(ptr);
            }
            finally
            {
                Marshal.ZeroFreeBSTR(passwordPtr);                
            }
        }
    }
}