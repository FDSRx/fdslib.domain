namespace FDSLib.Domain.Messages
{
    public class RegisterComputerMessage
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Workstation { get; set; }
        public string ApplicationFilter { get; set; }
        public string StoreId { get; set; }
    }
}