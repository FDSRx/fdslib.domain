﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Domain.Entities;

namespace FDSLib.Domain.Mappers
{
    public static class BusinessExtensions
    {
        /// <summary>
        /// Converts a domain Pharmacy object into a domain Store object.
        /// </summary>
        /// <param name="obj">Pharmacy object.</param>
        /// <returns></returns>
        public static Store ToStore(this Pharmacy obj)
        {
            if (obj == null)
            {
                return new Store();
            }

            var item = new Store()
                       {
                           Id = obj.Id,
                           Name = obj.Name,
                           ChainToken = obj.ChainToken,
                           StoreToken = obj.StoreToken,
                           Nabp = obj.Nabp,
                           PhoneNumber = obj.PhoneNumber,
                           PrimaryAddress = obj.PrimaryAddress,
                           EmailAddress = obj.EmailAddress,
                           MdmStoreKey = obj.MdmStoreKey,
                           Services = obj.Services,
                           TimeZone = obj.TimeZone,
                           Guid = obj.Guid,
                           Origin = obj.Origin
                       };

            return item;
        }





    }
}
