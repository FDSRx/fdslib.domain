﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FDSLib.Domain.Entities
{
    public interface IProviderUser
    {
        /// <summary>
        /// Gets or sets the username associated with the provider.
        /// </summary>
        string UserName { get; set; }

        /// <summary>
        /// Gets or sets the unencrypted password associated with the provider.
        /// </summary>
        string PasswordUnencrypted { get; set; }

        /// <summary>
        /// Gets or sets the encrypted password associated with the provider.
        /// </summary>
        string PasswordEncrypted { get; set; }
    }
}
