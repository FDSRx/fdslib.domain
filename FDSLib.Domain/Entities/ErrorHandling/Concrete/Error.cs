﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities
{
    public class Error
    {
        public string ErrorID { get; set; }
        public string ErrorCode { get; set; }
        public string Message { get; set; }
        public string FriendlyMessage { get; set; }
        public string Source { get; set; }
        public bool HasError { get; set; }
        public Exception Exception { get; set; }

        /// <summary>
        /// Parameterless constructor.
        /// </summary>
        public Error() { }

        /// <summary>
        /// Instantiates a new error object using a defined exception.
        /// </summary>
        /// <param name="ex">Exception object.</param>
        public Error(Exception ex)
        {
            this.ErrorID = Guid.NewGuid().ToString();
            this.ErrorCode = String.Empty;
            this.Message = ex.Message;
            this.FriendlyMessage = ex.InnerException.Message;
            this.Source = ex.Source;
            this.Exception = ex;
            this.HasError = true;
        }

    }
}
