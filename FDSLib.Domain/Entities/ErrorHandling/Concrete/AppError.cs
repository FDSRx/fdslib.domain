﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Common.Extensions;

namespace FDSLib.Domain.Entities
{
    public class AppError : BusinessEntity<string>, IAppError
    {
        #region IAppError Members

        /// <summary>
        /// A unique identifier that identifies the error.
        /// </summary>
        public new string Id { get; set; }

        /// <summary>
        /// A unique code that identifies the error.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// The source/origin of the error.
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// A technical message that describes the error.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// A friendly message that describes the message in little technical terms.
        /// </summary>
        public string FriendlyMessage { get; set; }

        #endregion

        public Exception Exception { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the AppError object.
        /// </summary>
        public AppError()
        {
        }

        /// <summary>
        /// Initializes a new instance of the AppError object.
        /// </summary>
        /// <param name="message">A technical and/or friendly message to describe the error</param>
        public AppError(string message)
        {
            this.Message = message;
            this.FriendlyMessage = message;
        }

        /// <summary>
        /// Initializes a new instance of the AppError object.
        /// </summary>
        /// <param name="message">A technical and/or friendly message to describe the error</param>
        /// <param name="ex">Exception object.</param>
        public AppError(string message, Exception ex) :
            this(message)
        {
            if (ex == null)
            {
                throw new ArgumentNullException("ex");
            }

            this.Source = ex.Source;
            this.Message = ex.Message;
            this.FriendlyMessage = ex.InnerException.Message;
        }

        /// <summary>
        /// Initializes a new instance of the AppError object.
        /// </summary>
        /// <param name="ex">Exception object.</param>
        public AppError(Exception ex) :
            this(null, ex)
        {

        }

        #endregion

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Message ?? this.FriendlyMessage ?? (this.Exception != null ? this.Exception.Message : String.Empty);
        }
    }
}
