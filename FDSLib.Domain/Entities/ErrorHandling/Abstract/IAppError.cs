﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities
{
    public interface IAppError
    {
        /// <summary>
        /// A unique identifier that identifies the error.
        /// </summary>
        string Id { get; set; }

        /// <summary>
        /// A unique code that identifies the error.
        /// </summary>
        string Code { get; set; }

        /// <summary>
        /// The source/origin of the error.
        /// </summary>
        string Source { get; set; }

        /// <summary>
        /// A technical message that describes the error.
        /// </summary>
        string Message { get; set; }

        /// <summary>
        /// A friendly message that describes the message in little technical terms.
        /// </summary>
        string FriendlyMessage { get; set; }

    }
    
}
