﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    public class ApplicationLog
    {
        /// <summary>
        /// A unique identifier of the log record.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Id { get; set; }

        /// <summary>
        /// A unique identifier of the application.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string ApplicationKey { get; set; }

        /// <summary>
        /// A unique identifier of a log entry type.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string LogEntryTypeKey { get; set; }

        /// <summary>
        /// The name of the machine invoking the log request.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string MachineName { get; set; }

        /// <summary>
        /// The name of the service invoking the log request.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string ServiceName { get; set; }

        /// <summary>
        /// The method or procedure invoking the log request.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string SourceName { get; set; }

        /// <summary>
        /// The event within the source invoking the log request (e.g. start of the procedure, end of the procedure, etc.).
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string EventName { get; set; }

        /// <summary>
        /// The name or user name of the individual invoking the request.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string UserName { get; set; }

        /// <summary>
        /// The start date/time of the event.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string StartTime { get; set; }

        /// <summary>
        /// The end date/time of the event.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string EndTime { get; set; }

        /// <summary>
        /// A descriptive message that describes the event.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Message { get; set; }

        /// <summary>
        /// A single or list of parameters that were used to invoke the method/procedure.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Arguments { get; set; }

        /// <summary>
        /// Gets/Sets a detailed explanation of the events that led to the error.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string StackTrace { get; set; }

        /// <summary>
        /// The date/time the record was created.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string DateCreated { get; set; }

        /// <summary>
        /// The date/time the record was modified.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string DateModified { get; set; }

        /// <summary>
        /// The name or username of the individual that created the record.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string CreatedBy { get; set; }

        /// <summary>
        /// The name or username of the individual that modified the record.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string ModifiedBy { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ApplicationLog object.
        /// </summary>
        public ApplicationLog()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ApplicationLog object.
        /// </summary>
        /// <param name="message">A descriptive message of the event.</param>
        public ApplicationLog(string message)
        {
            this.Message = message;
        }

        /// <summary>
        /// Initializes a new instance of the ApplicationLog object.
        /// </summary>
        /// <param name="logEntryTypeKey">The type of entry being recorded (e.g. informational, errorneous, warning, etc.).</param>
        /// <param name="message">A descriptive message of the event.</param>
        public ApplicationLog(string logEntryTypeKey, string message) :
            this(message)
        {
            this.LogEntryTypeKey = LogEntryTypeKey;
        }

        #endregion

        /// <summary>
        /// Returns the Message property of the object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Message;
        }
    }
}
