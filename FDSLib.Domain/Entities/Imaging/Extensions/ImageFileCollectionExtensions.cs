﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities
{
    public static class ImageFileCollectionExtensions
    {
        /// <summary>
        /// Finds a photo image by its unique id.
        /// </summary>
        /// <param name="collection">An enumerable collection of IImageFile objects.</param>
        /// <param name="id">Unique identifier of an image.</param>
        /// <returns></returns>
        public static PhotoImage FindById(this ImageFileCollection<PhotoImage> collection,  string id)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            return collection.FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Finds a photo image by its unique id.
        /// </summary>
        /// <param name="collection">An enumerable collection of IImageFile objects.</param>
        /// <param name="id">Unique identifier of an image.</param>
        /// <returns></returns>
        public static PhotoImage FindById(this IEnumerable<PhotoImage> collection, string id)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            return collection.FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Finds a photo image by its unique id. If the photo image is not found, then a new PhotoImage object will be returned.
        /// </summary>
        /// <param name="collection">An enumerable collection of IImageFile objects.</param>
        /// <param name="id">Unique identifier of an image.</param>
        /// <returns></returns>
        public static PhotoImage FindByIdOrNew(this IEnumerable<PhotoImage> collection, string id)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            return collection.FirstOrDefault(x => x.Id == id) ?? new PhotoImage();
        }

        /// <summary>
        /// Finds a photo by its file or uri location.
        /// </summary>
        /// <param name="collection">An enumerable collection of IImageFile objects.</param>
        /// <param name="filePath">Uri or file path of photo.</param>
        /// <returns></returns>
        public static PhotoImage FindByFilePath(this ImageFileCollection<PhotoImage> collection, string filePath)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            return collection.FirstOrDefault(x => x.FilePath == filePath);
        }

        /// <summary>
        /// Finds a photo by the unique key name identifier.
        /// </summary>
        /// <param name="collection">An enumerable collection of IImageFile objects.</param>
        /// <param name="keyName">Unique photo key.</param>
        /// <returns></returns>
        public static PhotoImage FindByKeyName(this ImageFileCollection<PhotoImage> collection, string keyName)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            return collection.FirstOrDefault(x => x.KeyName == keyName);
        }

        /// <summary>
        /// Finds a photo by the unique key name identifier.
        /// </summary>
        /// <param name="collection">An enumerable collection of IImageFile objects.</param>
        /// <param name="keyName">Unique photo key.</param>
        /// <returns></returns>
        public static PhotoImage FindByKeyName(this IEnumerable<PhotoImage> collection, string keyName)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            return collection.FirstOrDefault(x => x.KeyName == keyName);
        }

        /// <summary>
        /// Finds a photo by the unique key name identifier. If the photo image is not found, then a new PhotoImage object will be returned.
        /// </summary>
        /// <param name="collection">An enumerable collection of IImageFile objects.</param>
        /// <param name="keyName">Unique photo key.</param>
        /// <returns></returns>
        public static PhotoImage FindByKeyNameOrNew(this IEnumerable<PhotoImage> collection, string keyName)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            return collection.FirstOrDefault(x => x.KeyName == keyName) ?? new PhotoImage();
        }

        /// <summary>
        /// Returns the first image within the collection.  If there are no items within the collection, then a new PhotoImage object will be returned.
        /// </summary>
        /// <param name="collection">IEnumerable(Of PhotoImage) object.</param>
        /// <returns></returns>
        public static PhotoImage FirstOrNew(this IEnumerable<PhotoImage> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            var image = collection.FirstOrDefault();

            return image ?? new PhotoImage(); 
        }


    }
}
