﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities
{
    public class ImageFileCollection<T> : List<T>, IImageFileCollection<T>
        where T : IImageFile, new()
    {
        /// <summary>
        /// Finds an image by a specified file path.
        /// </summary>
        /// <param name="filePath">Uri or file path of image.</param>
        /// <returns></returns>
        public T FindByFilePath(string filePath)
        {
            return this.Where(x => x.FilePath == filePath).FirstOrDefault();
        }
    }
}
