﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities
{
    public interface IImageFile
    {
        /// <summary>
        /// Unique image identifier.
        /// </summary>
        string Id { get; set; }

        /// <summary>
        /// Name of file (e.g. MyPicture.jpeg).
        /// </summary>
        string FileName { get; set; }

        /// <summary>
        /// A file path or uri path that retrieves the picture.
        /// </summary>
        string FilePath { get; set; }

        /// <summary>
        /// The picture represented in its binary form.
        /// </summary>
        byte[] FileDataBinary { get; set; }

        /// <summary>
        /// The picture represented in a base64 encoded string.
        /// </summary>
        string FileDataString { get; set; }

        /// <summary>
        /// Date the image file was created.
        /// </summary>
        DateTime? DateCreated { get; set; }

        /// <summary>
        /// Date the image file was modified.
        /// </summary>
        DateTime? DateModified { get; set; }
    }
}
