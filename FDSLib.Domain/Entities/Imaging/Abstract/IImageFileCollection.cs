﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities
{
    public interface IImageFileCollection<T> 
        where T : IImageFile
    {
        /// <summary>
        /// Finds an image by a specified file path.
        /// </summary>
        /// <param name="filePath">Uri or file path of image.</param>
        /// <returns></returns>
        T FindByFilePath(string filePath);

    }
}
