﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities
{
    public interface IPhoto 
    {
        /// <summary>
        /// Unique file identifier.
        /// </summary>
        string Id { get; set; }

        /// <summary>
        /// Friendly Name of the picture.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Short, friendly, description of picture.
        /// </summary>
        string Caption { get; set; }

        /// <summary>
        /// Detailed description of picture item.
        /// </summary>
        string Description { get; set; }

    }
}
