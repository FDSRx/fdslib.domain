﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Common.Enumerations;
using FDSLib.Domain.Enumerations;

namespace FDSLib.Domain.Entities
{
    public class PhotoImage : ImageFile, IPhoto
    {
        /// <summary>
        /// Friendly Name of the picture.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Short, friendly, description of picture.
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Detailed description of picture item.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Unique file identifier.
        /// </summary>
        public string KeyName { get; set; }

        /// <summary>
        /// Height of digital photo.
        /// </summary>
        public string Height { get; set; }

        /// <summary>
        /// Width of digital photo.
        /// </summary>
        public string Width { get; set; }

        /// <summary>
        /// The language the image is inteded to represent (e.g. An image file for an advert could be created in Spanish).
        /// </summary>
        public Language Language { get; set; }

        /// <summary>
        /// Creates a new PhotoImage object.
        /// </summary>
        public PhotoImage()
        {
            this.Language = new Language(LanguageType.English.GetCode(), Enumerations.LanguageType.English.ToString());
        }

        /// <summary>
        /// Initializes a new instance of the PhotoImage object.
        /// </summary>
        /// <param name="image">IImageFile object.</param>
        public PhotoImage(IImageFile image)
        {
            if ((object)image == null)
            {
                throw new ArgumentNullException("image");
            }

            this.Id = image.Id;
            this.FilePath = image.FilePath;
            this.FileName = image.FileName;
            this.FileDataString = image.FileDataString;
            this.FileDataBinary = image.FileDataBinary;
            this.DateCreated = image.DateCreated;
            this.DateModified = image.DateModified;
        }

        /// <summary>
        /// Returns the name of the image.  Returns the file path of the image if a name is not provided. Returns the keyName of the image
        /// if a name or file path does not exist.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name ?? this.FilePath ?? this.KeyName;
        }
    }
}
