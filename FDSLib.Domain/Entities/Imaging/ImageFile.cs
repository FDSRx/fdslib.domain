﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace FDSLib.Domain.Entities
{
    public class ImageFile : ImageFileMetaData, IDisposable
    {
        /// <summary>
        /// Creates a new ImageFile object.
        /// </summary>
        public ImageFile() { }

        /// <summary>
        /// Creates a new ImageFile object
        /// </summary>
        /// <param name="imageData"></param>
        public ImageFile(byte[] imageData)
        {
            this.FileDataBinary = imageData;
            this.FileDataString = Convert.ToBase64String(imageData);
            this.DateCreated = DateTime.Now;
            this.DateModified = DateTime.Now;
        }

        /// <summary>
        /// Returns the Image meta data.
        /// </summary>
        /// <returns></returns>
        public IImageFile GetMetaData()
        {
            return new ImageFileMetaData(this);
        }

        /// <summary>
        /// Converts the file data string to a System.Drawing.Image object.
        /// </summary>
        /// <returns></returns>
        public Image StringDataToImage()
        {
            byte[] imageData = Convert.FromBase64String(this.FileDataString);

            MemoryStream ms = new MemoryStream(imageData);
            Image image = Image.FromStream(ms);
            ms.Close();
            ms.Dispose();

            return image;
        }

        /// <summary>
        /// Converts the file binary data to a System.Drawing.Image object.
        /// </summary>
        /// <returns></returns>
        public Image BinaryDataToImage()
        {
            byte[] imageData = this.FileDataBinary;

            MemoryStream ms = new MemoryStream(imageData);
            Image image = Image.FromStream(ms);
            ms.Close();
            ms.Dispose();

            return image;
        }

        /// <summary>
        /// Converts the file path string to a System.Drawing.Image object.
        /// </summary>
        /// <returns></returns>
        public Image FilePathToImage()
        {
            return new Bitmap(this.FilePath);
        }

        /// <summary>
        /// Releases all resources used by the ImageFile object.
        /// </summary>
        /// <param name="disposing">Indicates whether the object should be disposed.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.Id = null;
                this.FileDataBinary = null;
                this.FileDataString = null;
                this.FilePath = null;
                this.DateCreated = default(DateTime);
                this.DateModified = default(DateTime);
            }
        }

        /// <summary>
        /// Releases all resources used by the ImageFile object.
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
            this.Dispose(true);
        }

        /// <summary>
        /// Returns the file name of the image.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.FileName;
        }

        /// <summary>
        /// Creates a new ImageFile from base64 encoded string data.
        /// </summary>
        /// <param name="imageData">Base64 encoded string.</param>
        /// <returns></returns>
        public static ImageFile FromStringData(string imageData)
        {
            return new ImageFile() { FileDataString = imageData, FileDataBinary = Convert.FromBase64String(imageData) };
        }

        /// <summary>
        /// Creates a new ImageFile from base64 encoded string data. Returns a new instance of an ImageFile if the string is unable to be converted.
        /// </summary>
        /// <param name="imageData">Base64 encoded string.</param>
        /// <returns></returns>
        public static ImageFile FromStringDataOrDefault(string imageData)
        {
            ImageFile img;

            try
            {
                img = ImageFile.FromStringData(imageData);
            }
            catch
            {
                img = new ImageFile();
            }

            return img;
        }

        /// <summary>
        /// Creates a new ImageFile from base64 encoded string data. Returns a null instance if the string is unable to be converted.
        /// </summary>
        /// <param name="imageData">Base64 encoded string.</param>
        /// <returns></returns>
        public static ImageFile FromStringDataOrNull(string imageData)
        {
            ImageFile img = null;

            try
            {
                img = ImageFile.FromStringData(imageData);
            }
            catch
            {
                img = null;
            }

            return img;
        }

        /// <summary>
        /// Creates a new ImageFile from a System.Drawing.Image object.
        /// </summary>
        /// <param name="image">System.Drawing.Image object.</param>
        /// <returns></returns>
        public static ImageFile FromImage(Image image)
        {
            ImageFile imageFile = new ImageFile();

            byte[] results = null;

            using (MemoryStream ms = new MemoryStream())
            {
                ImageCodecInfo codec = ImageCodecInfo.GetImageEncoders().FirstOrDefault(c => c.FormatID == ImageFormat.Png.Guid);
                EncoderParameters jpegParms = new EncoderParameters(1);
                jpegParms.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 95L);
                image.Save(ms, codec, jpegParms);
                results = ms.ToArray();
            }

            imageFile.FileDataBinary = results;
            imageFile.FileDataString = Convert.ToBase64String(results);
            imageFile.DateCreated = DateTime.Now;
            imageFile.DateModified = DateTime.Now;

            return imageFile;
        }

        /// <summary>
        /// Resizes the image file.
        /// </summary>
        /// <param name="source">Image object.</param>
        /// <param name="width">Width to resize image.</param>
        /// <param name="height">Height to resize image.</param>
        /// <param name="maintainAspectRatio">Indicates whether the aspect ratio of the image should be maintained.</param>
        /// <returns></returns>
        public static ImageFile ResizeImage(Image source, int width, int height, bool maintainAspectRatio)
        {
            ImageFile imageFile = new ImageFile();

            byte[] results = null;

            if (source.Width <= width && source.Height <= height)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    ImageCodecInfo codec = ImageCodecInfo.GetImageEncoders().FirstOrDefault(c => c.FormatID == ImageFormat.Png.Guid);
                    EncoderParameters jpegParms = new EncoderParameters(1);
                    jpegParms.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 95L);
                    source.Save(ms, codec, jpegParms);
                    results = ms.ToArray();
                }

                imageFile.FileDataBinary = results;
                imageFile.FileDataString = Convert.ToBase64String(results);
                imageFile.DateCreated = DateTime.Now;
                imageFile.DateModified = DateTime.Now;

                return imageFile;
            }

            Bitmap image;

            try
            {
                int imgWith = width;
                int imgHeight = height;

                if (maintainAspectRatio)
                {
                    if (source.Width > source.Height)
                    {
                        imgWith = width;
                        imgHeight = (int)(source.Height * ((decimal)width / source.Width));
                    }
                    else
                    {
                        imgHeight = height;
                        imgWith = (int)(source.Width * ((decimal)height / source.Height));
                    }
                }

                image = new Bitmap(imgWith, imgHeight);

                using (Graphics g = Graphics.FromImage(image))
                {
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.FillRectangle(Brushes.Transparent, 0, 0, imgWith, imgHeight);
                    g.DrawImage(source, 0, 0, imgWith, imgHeight);
                }

                using (MemoryStream ms = new MemoryStream())
                {
                    ImageCodecInfo codec = ImageCodecInfo.GetImageEncoders().FirstOrDefault(c => c.FormatID == ImageFormat.Png.Guid);
                    EncoderParameters jpegParms = new EncoderParameters(1);
                    jpegParms.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 95L);
                    image.Save(ms, codec, jpegParms);
                    results = ms.ToArray();
                }

                imageFile.FileDataBinary = results;
                imageFile.FileDataString = Convert.ToBase64String(results);
                imageFile.DateCreated = DateTime.Now;
                imageFile.DateModified = DateTime.Now;


            }
            catch
            {
            }

            return imageFile;
        }

        /// <summary>
        /// Resizes the image file.
        /// </summary>
        /// <param name="source">Image object.</param>
        /// <param name="width">Width to resize image.</param>
        /// <param name="height">Height to resize image.</param>
        /// <returns></returns>
        public static ImageFile ResizeImage(Image source, int width, int height)
        {
            return ResizeImage(source, width, height, false);
        }

        /// <summary>
        /// Resizes the string image file.
        /// </summary>
        /// <param name="source">String image data.</param>
        /// <param name="width">Width to resize image.</param>
        /// <param name="height">Height to resize image.</param>
        /// <param name="maintainAspectRatio">Indicates whether the aspect ratio of the image should be maintained.</param>
        /// <returns></returns>
        public static ImageFile ResizeImage(string source, int width, int height, bool maintainAspectRatio)
        {
            ImageFile imageFile = new ImageFile();

            imageFile.FileDataString = source;

            return ResizeImage(imageFile.StringDataToImage(), width, height, maintainAspectRatio);
        }

        /// <summary>
        /// Resizes the string image file.
        /// </summary>
        /// <param name="source">String image data.</param>
        /// <param name="width">Width to resize image.</param>
        /// <param name="height">Height to resize image.</param>
        /// <returns></returns>
        public static ImageFile ResizeImage(string source, int width, int height)
        {
            return ResizeImage(source, width, height, false);
        }

        /// <summary>
        /// Resizes the binary image file.
        /// </summary>
        /// <param name="source">String image data.</param>
        /// <param name="width">Width to resize image.</param>
        /// <param name="height">Height to resize image.</param>
        /// <param name="maintainAspectRatio">Indicates whether the aspect ratio of the image should be maintained.</param>
        /// <returns></returns>
        public static ImageFile ResizeImage(byte[] source, int width, int height, bool maintainAspectRatio)
        {
            ImageFile imageFile = new ImageFile();

            imageFile.FileDataBinary = source;

            return ResizeImage(imageFile.BinaryDataToImage(), width, height, maintainAspectRatio);
        }

        /// <summary>
        /// Resizes the binary image file.
        /// </summary>
        /// <param name="source">String image data.</param>
        /// <param name="width">Width to resize image.</param>
        /// <param name="height">Height to resize image.</param>
        /// <returns></returns>
        public static ImageFile ResizeImage(byte[] source, int width, int height)
        {
            ImageFile imageFile = new ImageFile();

            imageFile.FileDataBinary = source;

            return ResizeImage(imageFile.BinaryDataToImage(), width, height, false);
        }


    }
}
