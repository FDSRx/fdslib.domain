﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Common.Enumerations;

namespace FDSLib.Domain.Entities
{
    public enum ImageKeyName
    {
        [EnumCode("PatientPictureSmall")]
        PatientPictureSmall,

        [EnumCode("PatientPictureOriginal")]
        PatientPictureOriginal,

        [EnumCode("PatientPictureTiny")]
        PatientPictureTiny,

        [EnumCode("ProfilePictureSmall")]
        ProfilePictureSmall,

        [EnumCode("ProfilePictureOriginal")]
        ProfilePictureOriginal,

        [EnumCode("ProfilePictureTiny")]
        ProfilePictureTiny,

    }
}
