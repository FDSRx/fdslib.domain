﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities
{
    public class ImageFileMetaData : Entity, IImageFile
    {

        /// <summary>
        /// Name of file (e.g. MyPicture.jpeg).
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// A file path or uri path that retrieves the picture.
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// The picture represented in its binary form.
        /// </summary>
        public byte[] FileDataBinary { get; set; }

        /// <summary>
        /// The picture represented in a base64 encoded string.
        /// </summary>
        public string FileDataString { get; set; }

        /// <summary>
        /// Date the image file was created.
        /// </summary>
        public DateTime? DateCreated { get; set; }

        /// <summary>
        /// Date the image file was modified.
        /// </summary>
        public DateTime? DateModified { get; set; }

        /// <summary>
        /// Initializes a new instance of the ImageFileMetaData object.
        /// </summary>
        protected ImageFileMetaData() { }

        /// <summary>
        /// Initializes a new instance of the ImageFileMetaData object.
        /// </summary>
        /// <param name="image">IImageFile object.</param>
        public ImageFileMetaData(IImageFile image)
        {
            this.Id = image.Id;
            this.FilePath = image.FilePath;
            this.FileName = image.FileName;
            this.FileDataString = image.FileDataString;
            this.FileDataBinary = image.FileDataBinary;
            this.DateCreated = image.DateCreated;
            this.DateModified = image.DateModified;
        }
    }
}
