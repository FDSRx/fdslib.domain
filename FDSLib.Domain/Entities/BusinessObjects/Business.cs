﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    [KnownType(typeof(BusinessEntity<long?>))]
    public class Business : BusinessEntity<long?>, IBusiness
    {
         /// <summary>
        /// The origin of the data.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public Origin Origin { get; set; }

        #region IBusiness Members

        /// <summary>
        /// A globally unique identifier of the business (GUID).
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string BusinessToken { get; set; }

        /// <summary>
        /// A unique number that identifies the business.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string BusinessNumber { get; set; }

        /// <summary>
        /// The name of the chain.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string Name { get; set; }

        #endregion

        /// <summary>
        /// The primary address of the chain.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public Address PrimaryAddress { get; set; }

        /// <summary>
        /// The latitude and longitude of the chain.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public Location Location { get; set; }

        /// <summary>
        /// The primary phone number of the chain.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// The primary email address of the chain.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string EmailAddress { get; set; }

        /// <summary>
        /// The primary fax number of the chain.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string FaxNumber { get; set; }

        /// <summary>
        /// The website of the business.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string Website { get; set; }

        /// <summary>
        /// The time zone of the business.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public TimeZone TimeZone { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the Business object.
        /// </summary>
        public Business()
        {
            this.PrimaryAddress = new Address();
            this.Location = new Location();
            this.Origin = new Origin();
            this.TimeZone = new TimeZone();
        }
        
        #endregion

        /// <summary>
        /// Returns the Name property of the object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}
