﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FDSLib.Domain.Entities.BusinessObjects.Strategy
{
    public class NullStoreEntityStrategy : IStoreEntityStrategy
    {

        /// <summary>
        /// The unique identifier of the entity.
        /// </summary>
        private readonly string _id;

        /// <summary>
        /// A globally unique identifier of the entity.
        /// </summary>
        private readonly Guid _guid;

        /// <summary>
        /// Unique identifier that identifies a specific store from a collection of stores.
        /// </summary>
        private readonly string _storeToken;

        /// <summary>
        /// Returns the store identification string.
        /// </summary>
        /// <returns></returns>
        public string GetId()
        {
            return this._id;
        }

        /// <summary>
        /// Returns the public store identification token.
        /// </summary>
        /// <returns></returns>
        public string GetStoreToken()
        {
            return this._storeToken;
        }

        /// <summary>
        /// Returns the globally unique store identifier.
        /// </summary>
        /// <returns></returns>
        public Guid GetGuid()
        {
            return this._guid;
        }

        /// <summary>
        /// Initializes a new instance of the NullStoreEntityStrategy class.
        /// </summary>
        public NullStoreEntityStrategy()
        {
            this._id = null;
            this._storeToken = null;
            this._guid = default(Guid);
        }

        /// <summary>
        /// Initializes a new instance of the NullStoreEntityStrategy class.
        /// </summary>
        /// <param name="id">No manipulation is performed.</param>
        public NullStoreEntityStrategy(string id)
        {
            this._id = id;
            this._storeToken = null;
            this._guid = default(Guid);
        }
    }
}