﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FDSLib.Domain.Entities.BusinessObjects.Strategy
{
    public interface IStoreEntityStrategy
    {
        /// <summary>
        /// Returns the store identification string.
        /// </summary>
        /// <returns></returns>
        string GetId();

        /// <summary>
        /// Returns the public store identification token.
        /// </summary>
        /// <returns></returns>
        string GetStoreToken();

        /// <summary>
        /// Returns the globally unique store identifier.
        /// </summary>
        /// <returns></returns>
        Guid GetGuid();
    }
}