﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FDSLib.Domain.Entities.BusinessObjects.Strategy
{
    public class StoreEntityStrategy : IStoreEntityStrategy
    {
        /// <summary>
        /// The unique identifier of the entity.
        /// </summary>
        private string _id;

        /// <summary>
        /// A globally unique identifier of the entity.
        /// </summary>
        private Guid _guid;

        /// <summary>
        /// Unique identifier that identifies a specific store from a collection of stores.
        /// </summary>
        private string _storeToken;

        /// <summary>
        /// Returns the store identification string.
        /// </summary>
        /// <returns></returns>
        public string GetId()
        {
            return this._id;
        }

        /// <summary>
        /// Returns the public store identification token.
        /// </summary>
        /// <returns></returns>
        public string GetStoreToken()
        {
            return this._storeToken;
        }

        /// <summary>
        /// Returns the globally unique store identifier.
        /// </summary>
        /// <returns></returns>
        public Guid GetGuid()
        {
            return this._guid;
        }

        /// <summary>
        /// Initializes a new instance of the DefaultSetEntityIdentifiers class.
        /// </summary>
        public StoreEntityStrategy()
        {
        }

        /// <summary>
        /// Initializes a new instance of the DefaultSetEntityIdentifiers class.
        /// </summary>
        /// <param name="id">Inspects the id and assigns it to its appropriate identity field. If the id is a guid, then both the Guid
        /// and id property will be populated with the same value. Otherwise, the Guid will have a random guid assigned and the id will be assigned the
        /// provided id.</param>
        public StoreEntityStrategy(string id)
        {
            Guid guid;

            if (Guid.TryParse(id, out guid))
            {
                this._storeToken = guid.ToString();
                this._guid = guid;
                this._id = id;
            }
            else
            {
                this._id = id;
                this._guid = Guid.NewGuid();
            }
        }


    }


}