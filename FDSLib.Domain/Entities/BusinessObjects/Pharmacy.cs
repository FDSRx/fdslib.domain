﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    public class Pharmacy : Store
    {

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the Pharmacy object.
        /// </summary>
        public Pharmacy() :
            base()
        {
        }
        
        #endregion

        /// <summary>
        /// Returns the Name property of the object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
