﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using FDSLib.Common.Helpers;

namespace FDSLib.Domain.Entities
{
    public class Person : Entity
    {

        /// <summary>
        /// A prefix of suffix to a person's name to identify his or her official position, profession, or veneration,
        /// or a form of address such as Mr, Mrs, or Ms.
        /// </summary>
        [DefaultValue(null)]
        public string Title { get; set; }

        /// <summary>
        /// A person's given first name.
        /// </summary>
        [DefaultValue(null)]
        public string FirstName { get; set; }

        /// <summary>
        /// A person's family name.
        /// </summary>
        [DefaultValue(null)]
        public string LastName { get; set; }

        /// <summary>
        /// The date the person was born.
        /// </summary>
        [DefaultValue(null)]
        public string BirthDate { get; set; }

        /// <summary>
        /// A person's name after the birth name and before the surname.
        /// </summary>
        [DefaultValue(null)]
        public string MiddleName { get; set; }

        /// <summary>
        ///  Follows a person's full name and provides additional information about the person.
        /// </summary>
        [DefaultValue(null)]
        public string Suffix { get; set; }

        /// <summary>
        /// Gender of person (i.e. Male or Female)
        /// </summary>
        [DefaultValue(null)]
        public Gender Gender { get; set; }

        /// <summary>
        /// A person's preferred speaking and reading language.
        /// </summary>
        [DefaultValue(null)]
        public Language Language { get; set; }

        /// <summary>
        /// A person's main email address.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryEmailAddress { get; set; }

        /// <summary>
        /// Indicates whether an email is allowed to be sent to the primary email address.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsEmailAllowedToPrimaryEmailAddress { get; set; }

        /// <summary>
        /// Indicates whether the primary email address is the person's preferred communication device.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsPrimaryEmailAddressPreferred { get; set; }

        /// <summary>
        /// A person's secondary email address.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string AlternateEmailAddress { get; set; }

        /// <summary>
        /// Indicates whether an email is allowed to be sent to the alternate email address.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsEmailAllowedToAlternateEmailAddress { get; set; }

        /// <summary>
        /// Indicates whether the alternate email address is the person's preferred communication device.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsAlternateEmailAddressPreferred { get; set; }

        /// <summary>
        /// The number in which a person can be reached when at their place of residence.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string HomePhone { get; set; }

        /// <summary>
        /// Indicates whether a call is allowed to be made to the person's home phone.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsCallAllowedToHomePhone { get; set; }

        /// <summary>
        /// Indicates whether a text is allowed to be sent to the person's home phone.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsTextAllowedToHomePhone { get; set; }

        /// <summary>
        /// Indicates whether the home phone is preferred communication device.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsHomePhonePreferred { get; set; }

        /// <summary>
        /// The number in which a person can be reached when they are mobile.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string MobilePhone { get; set; }

        /// <summary>
        /// Indicates whether a call is allowed to be made to the person's mobile phone.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsCallAllowedToMobilePhone { get; set; }

        /// <summary>
        /// Indicates whether a text is allowed to be sent to the person's home phone.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsTextAllowedToMobilePhone { get; set; }

        /// <summary>
        /// Indicates whether the mobile phone is preferred communication device.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsMobilePhonePreferred { get; set; }

        /// <summary>
        /// The number in which a person can be reached when they are at their place of work.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string WorkPhone { get; set; }

        /// <summary>
        /// Indicates whether a call is allowed to be made to the person's work phone.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsCallAllowedToWorkPhone { get; set; }

        /// <summary>
        /// Indicates whether a text is allowed to be sent to the person's home phone.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsTextAllowedToWorkPhone { get; set; }

        /// <summary>
        /// Indicates whether the work phone is preferred communication device.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsWorkPhonePreferred { get; set; }

        /// <summary>
        /// The number in which a person can be reached via fax.
        /// </summary>
        [DefaultValue(null)]
        public string FaxNumber { get; set; }

        /// <summary>
        /// A person's primary residence.
        /// </summary>
        [DefaultValue(null)]
        public Address PrimaryAddress { get; set; }

        /// <summary>
        /// A list of known mailing addresses in which the person uses (e.g. home address, work address, shipping address, etc.).
        /// </summary>
        [DefaultValue(null)]
        public List<Address> MailingAddresses { get; set; }

        #region Constructors

        /// <summary>
        /// Instantiates a new Person object.
        /// </summary>
        public Person()
        {
            this.PrimaryAddress = new Address();
            this.MailingAddresses = new List<Address>();
            this.Language = new Language();
            this.Gender = new Gender();
        }

        /// <summary>
        /// Instantiates a new Person object.
        /// </summary>
        /// <param name="firstName">A person's given first name.</param>
        /// <param name="lastName">A person's family name.</param>
        /// <param name="birthDate">The date the person was born.</param>
        public Person(string firstName, string lastName, string birthDate)
            : this()
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.BirthDate = birthDate;
        }

        /// <summary>
        /// Instantiates a new Person object.
        /// </summary>
        /// <param name="firstName">A person's given first name.</param>
        /// <param name="lastName">A person's family name.</param>
        public Person(string firstName, string lastName)
            : this()
        {
            this.FirstName = firstName;
            this.LastName = lastName;
        }


        #endregion

        /// <summary>
        /// Generates a name string formatted as first name/last name, or last name/first name.
        /// </summary>
        /// <param name="isFirstNameLast">Specifies if the first name is to appear after the last name separated by a comma.</param>
        /// <returns>A name formatted as Ms. Jane Doe, or as Doe, Jane.</returns>
        public string GetFullName(bool isFirstNameLast)
        {
            return isFirstNameLast ?
                string.Format("{0}{1}{2}", this.LastName, ", ", this.FirstName) :
                string.Format("{0}{1}{2}{3}", string.IsNullOrEmpty(this.Title) ? "" : this.Title + " ", this.FirstName, " ", this.LastName);
        }

        /// <summary>
        /// Generates a name string formatted as first name/last name, or last name/first name.
        /// </summary>
        /// <returns>A name formatted as Ms. Jane Doe.</returns>
        public string GetFullName()
        {
            return this.GetFullName(false);
        }

        /// <summary>
        /// Gets the age of the person. If a birth date is not available, or the age is not able to calculate, then -1 will be returned.
        /// </summary>
        /// <returns></returns>
        public int GetAge()
        {
            DateTime birthDate;
            DateTime now = DateTime.Now;

            if (this.BirthDate != null && StringHelper.IsDate(this.BirthDate))
            {
                birthDate = Convert.ToDateTime(this.BirthDate);
            }
            else
            {
                return -1;
            }

            int age = now.Year - birthDate.Year;
            if (now.Month < birthDate.Month || (now.Month == birthDate.Month && now.Day < birthDate.Day)) age--;

            return age;
        }

        /// <summary>
        /// Returns a person's name string formatted as first name/last name (e.g. John Doe).
        /// </summary>
        /// <returns>A person's name formatted as John Doe.</returns>
        public override string ToString()
        {
            return String.Format("{0}{1}{2}", this.FirstName, " ", this.LastName);
        }


    }

}