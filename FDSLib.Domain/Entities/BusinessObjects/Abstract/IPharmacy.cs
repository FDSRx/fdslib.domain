﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FDSLib.Domain.Entities
{
    public interface IPharmacy
    {
        /// <summary>
        /// The store's National Association Boards of Pharmacy number.
        /// </summary>
        string Nabp { get; set; }
    }
}