﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities
{
    public interface IBusiness
    {
        /// <summary>
        /// An internal unique identifier of the business.
        /// </summary>
        long? Id { get; set; }

        /// <summary>
        /// A globally unique identifier of the business (GUID).
        /// </summary>
        string BusinessToken { get; set; }

        /// <summary>
        /// A unique number that identifies the business.
        /// </summary>
        string BusinessNumber { get; set; }

        /// <summary>
        /// The name of the business.
        /// </summary>
        string Name { get; set; }
    }
}
