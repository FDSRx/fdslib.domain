﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities
{
    /// <summary>
    /// Generic interface that adds a strongly typed Id (object identifier) and a System.Guid as a universal object identifier.
    /// </summary>
    /// <typeparam name="T">The type of Id</typeparam>
    public interface IBusinessEntity<T>
    {
        /// <summary>
        /// The unique identifier of the entity.
        /// </summary>
        T Id { get; set; }

        /// <summary>
        /// A globally unique identifier of the entity.
        /// </summary>
        Guid Guid { get; set; }
    }
}
