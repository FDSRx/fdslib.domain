﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FDSLib.Domain.Entities
{
    public interface IStore
    {
        /// <summary>
        /// Name used to identify the business.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Unique identifier that identifies the holding company for the collection of stores.
        /// </summary>
        string ChainToken { get; set; }

        /// <summary>
        /// Unique identifier that identifies a specific store from a collection of stores.
        /// </summary>
        string StoreToken { get; set; }
    }
}