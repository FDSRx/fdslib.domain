﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Domain.Enumerations;


namespace FDSLib.Domain.Entities.BusinessObjects
{
    public class ServiceConnection
    {
        /// <summary>
        /// Account key used to connect to service software (e.g. username, login, etc.).
        /// </summary>
        public string AccountKey { get; set; }

        /// <summary>
        /// Indicates whether the patient has the service enabled.
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// The current connection status of the service user.
        /// </summary>
        public ConnectionStatus Status { get; set; }

        /// <summary>
        /// The date/time the mPC contract was enabled.
        /// </summary>
        public DateTime? DateEnabled { get; set; }

        /// <summary>
        /// Initializes a new instance of the ServiceConnection object.
        /// </summary>
        public ServiceConnection()
        {
        }
    }


}
