﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    public class Customer : Person
    {
        /// <summary>
        /// Primary institution in which the patient belongs.
        /// </summary>
        [DefaultValue(null)]
        public virtual Store Store { get; set; }

        /// <summary>
        /// Gets/Sets a list of applicable services.
        /// </summary>
        [DefaultValue(null)]
        public virtual List<Service> Services { get; set; }

        /// <summary>
        /// Gets / Sets an enumerable list of images that are applicable to a patient.
        /// </summary>
        [DefaultValue(null)]
        public virtual List<PictureFile> PictureFiles { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the Customer object.
        /// </summary>
        public Customer()
        {
            this.Store = new Store();
            this.Services = new List<Service>();
            this.PictureFiles = new List<PictureFile>();
        }

        #endregion
    }
}
