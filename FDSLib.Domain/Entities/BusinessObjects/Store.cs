﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using FDSLib.Domain.Entities.BusinessObjects.Strategy;

namespace FDSLib.Domain.Entities
{
    public class Store : Entity, IStore, IPharmacy
    {
        #region IStore Members

        /// <summary>
        /// Name of the institution.
        /// </summary>
        [DefaultValue(null)]
        public string Name { get; set; }

        /// <summary>
        /// Public unique identifier.
        /// </summary>
        [DefaultValue(null)]
        public string StoreToken { get; set; }

        /// <summary>
        /// The store's chain public unique identifier.
        /// </summary>
        [DefaultValue(null)]
        public string ChainToken { get; set; }

        #endregion

        #region IPharmacy Members

        /// <summary>
        /// The store's National Association Boards of Pharmacy number.
        /// </summary>
        [DefaultValue(null)]
        public string Nabp { get; set; }

        #endregion

        /// <summary>
        /// Determines the origin of the store data and provides its source data key.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public Origin Origin { get; set; }

        /// <summary>
        /// The data mart's store identifier.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public virtual string MdmStoreKey { get; set; }

        /// <summary>
        /// Primary location of the institution.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public Address PrimaryAddress { get; set; }

        /// <summary>
        /// The phone number of the institution.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// The email address of the institution.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string EmailAddress { get; set; }

        /// <summary>
        /// The time zone the institution is located.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string TimeZone { get; set; }

        /// <summary>
        /// Gets or sets a flag to indicate whether the business supports daylight savings time.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public bool SupportDST { get; set; }

        /// <summary>
        /// Gets or sets detailed information about the time zone of the store.
        /// </summary>
        public TimeZone TimeLocality { get; set; }

        /// <summary>
        /// Gets/Sets a list of services that are applicable to the store.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public virtual List<Service> Services { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the Store object.
        /// </summary>
        public Store()
        {
            this.Origin = new Origin();
            this.PrimaryAddress = new Address();
            this.Services = new List<Service>();
            this.TimeLocality = new TimeZone();
        }

        /// <summary>
        /// Initializes a new instance of the Store object.
        /// </summary>
        /// <param name="id">A unique identifier that identifies the store. The identifier may be a store token or id.</param>
        public Store(string id) :
            this()
        {
            SetStoreEntity(new StoreEntityStrategy(id));
        }

        #endregion

        #region Strategies

        /// <summary>
        /// Initializes or re-calculates the store entity identifiers.
        /// </summary>
        /// <param name="strategy"></param>
        private void SetStoreEntity(IStoreEntityStrategy strategy)
        {
            this.Id = strategy.GetId();
            this.StoreToken = strategy.GetStoreToken();
            this.Guid = strategy.GetGuid();
        }

        #endregion

        /// <summary>
        /// Returns the name of the store.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("{0}", this.Name);
        }
    }
}