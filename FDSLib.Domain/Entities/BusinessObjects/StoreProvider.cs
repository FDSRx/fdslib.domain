﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    [Serializable]
    public class StoreProvider : BusinessEntity<string>, IProviderUser
    {
        /// <summary>
        /// Gets or sets the store.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public Store Store { get; set; }

        /// <summary>
        /// Gets or sets the provider.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public Provider Provider { get; set; }

        /// <summary>
        /// Gets or sets the username associated with the provider.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the unencrypted password associated with the provider.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string PasswordUnencrypted { get; set; }

        /// <summary>
        /// Gets or sets the encrypted password associated with the provider.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string PasswordEncrypted { get; set; }

        /// <summary>
        /// Gets or sets the date/time the item was created.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string DateCreated { get; set; }

        /// <summary>
        /// Gets or sets the date/time the item was modified.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string DateModified { get; set; }

        /// <summary>
        /// Gets or sets the name or username of the individual that created the item.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the name or username of the individual that modified the item.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string ModifiedBy { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the StoreProvider object.
        /// </summary>
        public StoreProvider()
        {
            this.Store = new Store();
            this.Provider = new Provider();
        }

        #endregion

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("Store: {0} {1} | Provider: {2} ({3}) | Username: {4}",
                this.Store != null ? this.Store.Name : null,
                this.Store != null ? this.Store.Nabp : null,
                this.Provider != null ? this.Provider.Name : null,
                this.Provider != null ? this.Provider.Code : null,
                this.UserName);
        }

    }
}
