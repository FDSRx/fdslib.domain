﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FDSLib.Domain.Entities
{
    [XmlRoot(ElementName = "Stores")]
    [CollectionDataContract(Name = "Stores", ItemName = "Store")]
    public class StoreList : List<Store>
    {
    }
}
