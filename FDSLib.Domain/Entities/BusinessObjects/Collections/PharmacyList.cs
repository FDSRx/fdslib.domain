﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FDSLib.Domain.Entities
{
    [XmlRoot(ElementName = "Pharmacies")]
    [CollectionDataContract(Name = "Pharmacies", ItemName = "Pharmacy")]
    public class PharmacyList : List<Pharmacy>
    {
    }
}
