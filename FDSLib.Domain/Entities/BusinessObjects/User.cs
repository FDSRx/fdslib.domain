﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities
{
    public class User : Person
    {
        /// <summary>
        /// A unique system identifier.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// A unique token assigned to a user that allows access to data.
        /// </summary>
        public string UserToken { get; set; }

        /// <summary>
        /// A friendly name that identifies the user.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// An enumerable list of roles.
        /// </summary>
        public List<Role> Roles { get; set; }

        /// <summary>
        /// The date/time the user was last (or currently) logged into the applicable application(s).
        /// </summary>
        public DateTime? DateLastLoggedIn { get; set; }

        /// <summary>
        /// The date/time the user last interacted with the application.
        /// </summary>
        public DateTime? DateLastActive { get; set; }

        /// <summary>
        /// The date/time the user was last locked out of his/hser account.
        /// </summary>
        public DateTime? DateLastLockedOut { get; set; }

        /// <summary>
        /// The date/time the user last changed his/her password.
        /// </summary>
        public DateTime? DateLastPasswordChanged { get; set; }

        /// <summary>
        /// Gets / Sets an enumerable list of images that are applicable to a patient.
        /// </summary>
        public virtual List<PictureFile> PictureFiles { get; set; }

        /// <summary>
        /// Initializes a new instance of the User object.
        /// </summary>
        public User()
        {
            this.Roles = new List<Role>();
            this.PictureFiles = new List<PictureFile>();
        }

        /// <summary>
        /// Indicates whether the user has the provided role.
        /// </summary>
        /// <param name="role">A system role.</param>
        /// <returns></returns>
        public bool IsInRole(string role)
        {
            bool blnIsInRole = false;

            if (this.Roles != null)
            {
                if (this.Roles.Find(x => x.Code == role) != null)
                {
                    blnIsInRole = true;
                }
            }

            return blnIsInRole;
        }

        /// <summary>
        /// Returns the name of the User object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString();
        }



    }



}
