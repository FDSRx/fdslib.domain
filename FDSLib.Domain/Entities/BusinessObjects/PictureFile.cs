﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    public class PictureFile : BusinessEntity<long?>
    {
        /// <summary>
        /// The application in which the picture is associated.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public Application Application { get; set; }

        /// <summary>
        /// The business in which the picture is associated.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public Business Business { get; set; }

        /// <summary>
        /// The person in which the picture is associated.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public Person Person { get; set; }

        /// <summary>
        /// A unique key that identifies the image.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string KeyName { get; set; }

        /// <summary>
        /// The name of the image.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Name { get; set; }

        /// <summary>
        /// A short description of the image.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Caption { get; set; }

        /// <summary>
        /// A description of the image.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Description { get; set; }

        /// <summary>
        /// The name of the image file.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string FileName { get; set; }

        /// <summary>
        /// The name of the image file in a string replacement ordinal format.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string FileNameOrdinalFormat { get; set; }

        /// <summary>
        /// The name of the image file in a string replacement named format.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string FileNameNamedFormat { get; set; }

        /// <summary>
        /// A file system or uri that points to the image file.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string FilePath { get; set; }

        /// <summary>
        /// A file system or uri that points to the image file in a string replacement ordinal format.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string FilePathOrdinalFormat { get; set; }

        /// <summary>
        /// A file system or uri that points to the image file in a string replacement named format.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string FilePathNamedFormat { get; set; }

        /// <summary>
        /// The System.String representation of the image file (typically represented in Base64 encoded format).
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string FileDataString { get; set; }

        /// <summary>
        /// The image file represented in its binary format.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public byte[] FileDataBinary { get; set; }

        /// <summary>
        /// The language in which the image is described (i.e. english, spanish, french, etc.).
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public Language Language { get; set; }

        /// <summary>
        /// The date/time the image was created.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public DateTime? DateCreated { get; set; }

        /// <summary>
        /// The date/time the image was modified.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public DateTime? DateModified { get; set; }

        /// <summary>
        /// The name or username of the individual that created the image.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string CreatedBy { get; set; }

        /// <summary>
        /// The name or username of the individual that modified the image.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string ModifiedBy { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the PictureFile object.
        /// </summary>
        public PictureFile()
        {
            this.Application = new Application();
            this.Business = new Business();
            this.Person = new Person();
            this.Language = new Language();
        }

        #endregion

        /// <summary>
        /// Returns the name of the image file.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }

    }

}
