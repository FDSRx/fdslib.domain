﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class PharmacyRequest : StoreRequest
    {
        /// <summary>
        /// A single or comma delimited list of National Association of Boards of Pharmacy unique identification numbers.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string Nabp { get; set; }

        #region Constructors

        /// <summary>
        /// Instantiates a new instance of the PharmacyRequest object.
        /// </summary>
        public PharmacyRequest()
        {
        }

        /// <summary>
        /// Instantiates a new instance of the PharmacyRequest object.
        /// </summary>
        /// <param name="applicationKey">A unique key that identifies an application.</param>
        /// <param name="businessKey">A single or comma delimited list of business unique identifiers.</param>
        public PharmacyRequest(string applicationKey, string businessKey)
        {
            this.ApplicationKey = applicationKey;
            this.BusinessKey = businessKey;
        }

        #endregion

        /// <summary>
        /// Returns the System.String representation of the request.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("Appliation: {0} | Business: {1} | Business Type: {2}",
                this.ApplicationKey ?? "N/A", this.BusinessKey ?? "N/A", this.BusinessKeyType ?? "N/A");
        }
    }
}
