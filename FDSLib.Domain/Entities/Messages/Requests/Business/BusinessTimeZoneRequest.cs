﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    public class BusinessTimeZoneRequest : BusinessRequest
    {

        /// <summary>
        /// Gets or sets the "use default" indicator.  True to use the system specified default time zone; otherwise, false
        /// to return a null value if a time zone cannot be discovered.
        /// </summary>
        public bool UseDefaultTimeZoneIfNotFound { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the BusinessTimeZoneRequest object.
        /// </summary>
        public BusinessTimeZoneRequest()
        {
        }

        #endregion

        /// <summary>
        /// Returns the String representation of the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
