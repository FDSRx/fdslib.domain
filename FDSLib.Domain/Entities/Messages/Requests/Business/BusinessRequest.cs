﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class BusinessRequest : BasicRequest
    {
        /// <summary>
        /// The name or partial name of a business.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string BusinessName { get; set; }

        #region Constructors

        /// <summary>
        /// Instantiates a new instance of the BusinessRequest object.
        /// </summary>
        public BusinessRequest()
        {
        }

        /// <summary>
        /// Instantiates a new instance of the BusinessRequest object.
        /// </summary>
        /// <param name="businessKey">A single or comma delimited list of business unique identifiers.</param>
        public BusinessRequest(string businessKey)
        {
            this.BusinessKey = businessKey;
        }

        /// <summary>
        /// Instantiates a new instance of the BusinessRequest object.
        /// </summary>
        /// <param name="applicationKey">A unique key that identifies an application.</param>
        /// <param name="businessKey">A single or comma delimited list of business unique identifiers.</param>
        public BusinessRequest(string applicationKey, string businessKey)
        {
            this.ApplicationKey = applicationKey;
            this.BusinessKey = businessKey;
        }

        #endregion

        /// <summary>
        /// Returns the System.String representation of the request.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("Appliation: {0} | Business: {1} | Business Type: {2}",
                this.ApplicationKey ?? "N/A", this.BusinessKey ?? "N/A", this.BusinessKeyType ?? "N/A");
        }
        
    }
}
