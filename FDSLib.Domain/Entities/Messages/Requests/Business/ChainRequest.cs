﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{

    [DataContract]
    public class ChainRequest : BusinessRequest
    {
        /// <summary>
        /// A single or comma delimited list of stores.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string StoreKey { get; set; }

        /// <summary>
        /// The type of store key that is being requested (i.e. Business ID, Source ID, etc.).
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string StoreKeyType { get; set; }

        /// <summary>
        /// A single or comma delimited list of chains.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string ChainKey { get; set; }

        /// <summary>
        /// The type of chain key that is being requested (i.e. Business ID, Source ID, etc.).
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string ChainKeyType { get; set; }

        #region Constructors

        /// <summary>
        /// Instantiates a new instance of the ChainRequest object.
        /// </summary>
        public ChainRequest()
        {
        }

        /// <summary>
        /// Instantiates a new instance of the ChainRequest object.
        /// </summary>
        /// <param name="applicationKey">A unique key that identifies an application.</param>
        /// <param name="businessKey">A single or comma delimited list of business unique identifiers.</param>
        public ChainRequest(string applicationKey, string businessKey)
        {
            this.ApplicationKey = applicationKey;
            this.BusinessKey = businessKey;
        }

        #endregion

        /// <summary>
        /// Returns the System.String representation of the request.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("Application: {0} | Business: {1} | Store: {2} | Chain: {3}",
                this.ApplicationKey,
                this.BusinessKey,
                this.StoreKey,
                this.ChainKey);
        }
    }



}
