﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class StoreRequest : BusinessRequest
    {
        /// <summary>
        /// A single or comma delimited list of stores.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string StoreKey { get; set; }

        /// <summary>
        /// A single or comma delimited list of applicable chains.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string ChainKey { get; set; }

        /// <summary>
        /// The type of chain key that is being requested (i.e. Business ID, Source ID, etc.).
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string ChainKeyType { get; set; }

        #region Constructors

        /// <summary>
        /// Instantiates a new instance of the StoreRequest object.
        /// </summary>
        public StoreRequest()
        {
        }

        /// <summary>
        /// Initializes a new instance of the StoreRequest object.
        /// </summary>
        /// <param name="storeKey">A unique store identifier.</param>
        public StoreRequest(string storeKey)
        {
            this.BusinessKey = storeKey;
        }

        /// <summary>
        /// Instantiates a new instance of the StoreRequest object.
        /// </summary>
        /// <param name="applicationKey">A unique key that identifies an application.</param>
        /// <param name="businessKey">A single or comma delimited list of business unique identifiers.</param>
        public StoreRequest(string applicationKey, string businessKey)
        {
            this.ApplicationKey = applicationKey;
            this.BusinessKey = businessKey;
        }

        #endregion

        /// <summary>
        /// Returns the System.String representation of the request.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("Appliation: {0} | Business: {1} | Business Type: {2}",
                this.ApplicationKey ?? "N/A", this.BusinessKey ?? "N/A", this.BusinessKeyType ?? "N/A");
        }
    }
}
