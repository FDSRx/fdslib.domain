﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    public class ModifyImageRequest : RequestBase
    {
        /// <summary>
        /// A single or comma delimited list of unique identifiers.
        /// </summary>
        [DefaultValue(null)]
        public string ImageKey { get; set; }

        /// <summary>
        /// A unique key that identifies the image.
        /// </summary>
        [DefaultValue(null)]
        public string KeyName { get; set; }

        /// <summary>
        /// The name of the image.
        /// </summary>
        [DefaultValue(null)]
        public string Name { get; set; }

        /// <summary>
        /// A short description of the image.
        /// </summary>
        [DefaultValue(null)]
        public string Caption { get; set; }

        /// <summary>
        /// A description of the image.
        /// </summary>
        [DefaultValue(null)]
        public string Description { get; set; }

        /// <summary>
        /// The name of the image file.
        /// </summary>
        [DefaultValue(null)]
        public string FileName { get; set; }

        /// <summary>
        /// A file system or uri that points to the image file.
        /// </summary>
        [DefaultValue(null)]
        public string FilePath { get; set; }

        /// <summary>
        /// The System.String representation of the image file (typically represented in Base64 encoded format).
        /// </summary>
        [DefaultValue(null)]
        public string FileDataString { get; set; }

        /// <summary>
        /// The image file represented in its binary format.
        /// </summary>
        [DefaultValue(null)]
        public byte[] FileDataBinary { get; set; }

        /// <summary>
        /// The date/time the image was created.
        /// </summary>
        [DefaultValue(null)]
        public DateTime? DateCreated { get; set; }

        /// <summary>
        /// The date/time the image was modified.
        /// </summary>
        [DefaultValue(null)]
        public DateTime? DateModified { get; set; }

        /// <summary>
        /// The name or username of the individual that created the image.
        /// </summary>
        [DefaultValue(null)]
        public string CreatedBy { get; set; }

        /// <summary>
        /// The name or username of the individual that modified the image.
        /// </summary>
        [DefaultValue(null)]
        public string ModifiedBy { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the AddImageRequest object.
        /// </summary>
        public ModifyImageRequest()
        {
        }

        #endregion

        /// <summary>
        /// Returns the file name of the image.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.FileName;
        }
    }
}
