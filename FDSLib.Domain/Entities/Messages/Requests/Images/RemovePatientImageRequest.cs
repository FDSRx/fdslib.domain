﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    public class RemovePatientImageRequest : ModifyImageRequest
    {
        /// <summary>
        /// The patient's unique identifier.
        /// </summary>
        [DefaultValue(null)]
        public string PatientKey { get; set; }

        /// <summary>
        /// The type of unique identifier that is provided to locate a patient.
        /// </summary>
        [DefaultValue(null)]
        public string PatientKeyType { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the RemovePatientImageRequest object.
        /// </summary>
        public RemovePatientImageRequest()
        {
        }

        /// <summary>
        /// Initializes a new instance of the RemovePatientImageRequest object.
        /// </summary>
        /// <param name="applicationKey">The application unique identifier.</param>
        /// <param name="businessKey">The business unique identifier.</param>
        /// <param name="patientKey">The patient's unique identifier.</param>
        public RemovePatientImageRequest(string applicationKey, string businessKey, string patientKey)
        {
            this.BusinessKey = businessKey;
            this.PatientKey = patientKey;
            this.ApplicationKey = applicationKey;
        }

        /// <summary>
        /// Initializes a new instance of the RemovePatientImageRequest object.
        /// </summary>
        /// <param name="businessKey">The business unique identifier.</param>
        /// <param name="patientKey">The patient's unique identifier.</param>
        public RemovePatientImageRequest(string businessKey, string patientKey)
        {
            this.BusinessKey = businessKey;
            this.PatientKey = patientKey;
        }

        #endregion

        public override string ToString()
        {
            return String.Format("BusinessKey: {0} | PatientKey: {1} | FileName: {2}", this.BusinessKey, this.PatientKey,
                this.FileName);
        }
    }
}
