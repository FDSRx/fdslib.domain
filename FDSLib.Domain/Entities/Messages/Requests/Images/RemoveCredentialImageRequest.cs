﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    public class RemoveCredentialImageRequest : ModifyImageRequest
    {
        /// <summary>
        /// The credentials's unique identifier.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string CredentialKey { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the RemoveCredentialImageRequest object.
        /// </summary>
        public RemoveCredentialImageRequest()
        {
        }

        /// <summary>
        /// Initializes a new instance of the RemoveCredentialImageRequest object.
        /// </summary>
        /// <param name="applicationKey">The application unique identifier.</param>
        /// <param name="businessKey">The business unique identifier.</param>
        /// <param name="credentialKey">The user's unique identifier.</param>
        public RemoveCredentialImageRequest(string applicationKey, string businessKey, string credentialKey)
        {
            this.BusinessKey = businessKey;
            this.CredentialKey = credentialKey;
            this.ApplicationKey = applicationKey;
        }

        /// <summary>
        /// Initializes a new instance of the RemoveCredentialImageRequest object.
        /// </summary>
        /// <param name="businessKey">The business unique identifier.</param>
        /// <param name="credentialKey">The user's unique identifier.</param>
        public RemoveCredentialImageRequest(string businessKey, string credentialKey)
        {
            this.BusinessKey = businessKey;
            this.CredentialKey = credentialKey;
        }

        #endregion

        public override string ToString()
        {
            return String.Format("BusinessKey: {0} | PatientKey: {1} | FileName: {2}", this.BusinessKey, this.CredentialKey,
                this.FileName);
        }
    }
}
