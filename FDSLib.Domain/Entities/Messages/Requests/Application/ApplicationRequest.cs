﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class ApplicationRequest : RequestBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ApplicationRequest object.
        /// </summary>
        public ApplicationRequest()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ApplicationRequest object.
        /// </summary>
        public ApplicationRequest(string applicationKey)
        {
            this.ApplicationKey = applicationKey;
        }

        #endregion

        /// <summary>
        /// Returns the ApplicationKey property of the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.ApplicationKey;
        }
    }
}
