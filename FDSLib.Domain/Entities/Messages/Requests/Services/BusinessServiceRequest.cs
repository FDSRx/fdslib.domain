﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class BusinessServiceRequest : BusinessRequest
    {

        /// <summary>
        /// A single or comma delimited list of unique service identifiers.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string ServiceKey { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the BusinessServiceRequest object.
        /// </summary>
        public BusinessServiceRequest()
        {
        }

        #endregion

        /// <summary>
        /// Returns the System.String representation of the object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString();
        }

    }
}
