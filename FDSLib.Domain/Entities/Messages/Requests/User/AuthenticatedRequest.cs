﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class AuthenticatedRequest : AuthenticatedRequestBase
    {
        /// <summary>
        /// Instantiates a new instance of the AuthenticatedRequest object.
        /// </summary>
        public AuthenticatedRequest()
        {
        }

        /// <summary>
        /// Instantiates a new instance of the AuthenticatedRequest object.
        /// </summary>
        /// <param name="businessKey">Unique business identifier.</param>
        /// <param name="applicationKey">Unique key used to identify an application.</param>
        /// <param name="authenticationToken">The unique security token distributed to a client.</param>
        public AuthenticatedRequest(string applicationKey, string businessKey, string authenticationToken) :
            base(applicationKey, businessKey, authenticationToken)
        {
        }

        /// <summary>
        /// Returns the authentication token string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.AuthenticationKey ?? this.CredentialKey;
        }
    }
}
