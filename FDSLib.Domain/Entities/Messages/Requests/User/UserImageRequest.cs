﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Common.Enumerations;
using FDSLib.Domain.Enumerations;

namespace FDSLib.Domain.Entities.Messages
{
    public class UserImageRequest : UserRequest
    {
         /// <summary>
        /// A single or comma delimited list of unique identifiers.
        /// </summary>
        public string ImageKey { get; set; }

        /// <summary>
        /// The unique key that identifies a particular image.
        /// </summary>
        public string KeyName { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the UserImageRequest object.
        /// </summary>
        public UserImageRequest()
        {
            this.LanguageKey = CultureName.English.GetCode();
        }

        /// <summary>
        /// Initializes a new instance of the UserImageRequest object.
        /// </summary>
        /// <param name="businessKey">The business unique identifier.</param>
        /// <param name="credentialKey">The credential's (user's) unique identifier.</param>
        public UserImageRequest(string businessKey, string credentialKey)
        {
            this.BusinessKey = businessKey;
            this.CredentialKey = credentialKey;
        }

        #endregion

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
