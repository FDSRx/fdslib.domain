﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class CreateCredentialRequest
    {
        /// <summary>
        /// A single or comma delimited list of applications to apply to the user.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string ApplicationKey { get; set; }

        /// <summary>
        /// The unique identifier of the business to apply to the user.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string BusinessKey { get; set; }

        /// <summary>
        /// The type of business key that is being requested (i.e. NABP, BusinessID, Source Store ID, etc.).
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string BusinessKeyType { get; set; }

        /// <summary>
        /// A single or comma delimited list of roles to apply to the user.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string RoleKey { get; set; }

        /// <summary>
        /// A unique name that identifies the user.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Username { get; set; }

        /// <summary>
        /// An alpha-numeric string that accompanies the username.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Password { get; set; }

        /// <summary>
        /// Indicates whether the account should be declared as being approved for use.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsApproved { get; set; }

        /// <summary>
        /// Indicates whether the account should be declared as needing the password changed upon authenticating.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsChangePasswordRequired { get; set; }

        /// <summary>
        /// The date/time the password should expire.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string DatePasswordExpires { get; set; }

        /// <summary>
        /// Indicates whether the account should be declared as being disabled until further notice.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsDisabled { get; set; }

        /// <summary>
        /// A prefix of suffix to a person's name to identify his or her official position, profession, or veneration,
        /// or a form of address such as Mr, Mrs, or Ms.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        /// <summary>
        /// A person's given first name.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string FirstName { get; set; }

        /// <summary>
        /// A person's family name.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string LastName { get; set; }

        /// <summary>
        /// The date the person was born.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string BirthDate { get; set; }

        /// <summary>
        /// A person's name after the birth name and before the surname.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string MiddleName { get; set; }

        /// <summary>
        ///  Follows a person's full name and provides additional information about the person.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Suffix { get; set; }

        /// <summary>
        /// Gender of person (i.e. Male or Female)
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string GenderKey { get; set; }

        /// <summary>
        /// A person's preferred speaking and reading language.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string LanguageKey { get; set; }

        /// <summary>
        /// A person's main email address.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string PrimaryEmailAddress { get; set; }

        /// <summary>
        /// Indicates whether an email is allowed to the specified address.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsEmailAllowedToPrimaryEmailAddress { get; set; }

        /// <summary>
        /// A person's secondary email address.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string AlternateEmailAddress { get; set; }

        /// <summary>
        /// Indicates whether an email is allowed to the specified address.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsEmailAllowedToAlternateEmailAddress { get; set; }

        /// <summary>
        /// The number in which a person can be reached when at their place of residence.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string HomePhone { get; set; }

        /// <summary>
        /// Indicates whether a voice call is allowed to the specified address.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsCallAllowedToHomePhone { get; set; }

        /// <summary>
        /// Indicates whether a text message is allowed to the specified address.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsTextMessageAllowedToHomePhone { get; set; }

        /// <summary>
        /// The number in which a person can be reached when they are mobile.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string MobilePhone { get; set; }

        /// <summary>
        /// Indicates whether a voice call is allowed to the specified address.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsCallAllowedToMobilePhone { get; set; }

        /// <summary>
        /// Indicates whether a text message is allowed to the specified address.
        /// </summary>
        [DefaultValue(false)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsTextMessageAllowedToMobilePhone { get; set; }

        /// <summary>
        /// The number in which a person can be reached when they are at their place of work.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string WorkPhone { get; set; }

        /// <summary>
        /// The number in which a person can be reached via fax.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string FaxNumber { get; set; }

        /// <summary>
        /// The information identifying the exact address within the given Postal Code such as a street name
        /// and number or P.O. Box.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Supplemental address information such as Suite number, Apartment number, etc.  Used to further qualify a mailing address.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string AddressLine2 { get; set; }

        /// <summary>
        /// A specific incorporated municipality.  Commonly known as a city, town, or village.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string City { get; set; }

        /// <summary>
        /// A territorial subdivision of a country having a central civil government or authority.  
        /// Commonly known as states, regions, or provinces.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string State { get; set; }

        /// <summary>
        /// A code utilized to further qualify an address for the purpose of sorting and filtering mail.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string PostalCode { get; set; }

        /// <summary>
        /// The name or username of the individual that made the request.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string CreatedBy { get; set; }

        #region Constructors

        /// <summary>
        /// Initailizes a new instance of the CreateCredentialRequest object.
        /// </summary>
        public CreateCredentialRequest()
        {
        }

        #endregion

        /// <summary>
        /// Returns the Username property of the object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("{0}", this.Username);
        }
    }
}
