﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class UpdateExtranetUserRequest : CreateCredentialRequest
    {
        /// <summary>
        /// The unique identifier of the user.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string CredentialKey { get; set; }

        /// <summary>
        /// Indicates whether the update process should delete all items that are not specified in the request.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public bool DeleteUnspecifiedRoles { get; set; }

        /// <summary>
        /// The name or username of the individual that modified the object.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string ModifiedBy { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new Instance of the UpdateExtranetUserRequest object.
        /// </summary>
        public UpdateExtranetUserRequest()
        {
        }

        /// <summary>
        /// Initializes a new Instance of the UpdateExtranetUserRequest object.
        /// </summary>
        /// <param name="credentialKey">The unique identifier of the user.</param>
        public UpdateExtranetUserRequest(string credentialKey)
        {
            this.CredentialKey = credentialKey;
        }

        #endregion

        /// <summary>
        /// Returns the CredentialKey property of the object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.CredentialKey;
        }




    }
}
