﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using FDSLib.Common.Extensions;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class LinkedProviderUserRequest : UserRequest
    {
        /// <summary>
        /// The unique identifier of the credential.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string CredentialProviderKey { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the LinkedProviderUserRequest object.
        /// </summary>
        public LinkedProviderUserRequest()
        {
        }

        /// <summary>
        /// Initializes a new instance of the LinkedProviderUserRequest object.
        /// </summary>
        /// <param name="businessKey">A unique business identifier.</param>
        /// <param name="credentialKey">A unique credential identifier.</param>
        /// <param name="providerKey">A unique provider identifier (e.g. id or code).</param>
        public LinkedProviderUserRequest(string businessKey, string credentialKey, string providerKey)
        {
            this.BusinessKey = businessKey;
            this.CredentialKey = credentialKey;
            this.CredentialProviderKey = providerKey;
        }

        #endregion

        /// <summary>
        /// Returns the String representation of the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("Provider: {0} | Application: {1} | Business: {2} | Credential: {3} | User: {4} | Password: {5}",
                this.CredentialProviderKey,
                this.ApplicationKey,
                this.BusinessKey,
                this.CredentialKey.IfNullOrEmpty(this.AuthenticationKey),
                this.Username,
                this.Password);
        }


    }
}
