﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    public class UserRequest : AuthenticatedRequestBase
    {
        /// <summary>
        /// The user's given first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// The user's given last name.
        /// </summary>
        public string LastName { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the UserRequest object.
        /// </summary>
        public UserRequest()
        {
        }

        /// <summary>
        /// Initializes a new instance of the UserRequest object.
        /// </summary>
        /// <param name="businessKey">The unique store identifier.</param>
        /// <param name="userKey">the unique user identifier.</param>
        public UserRequest(string businessKey, string userKey)
        {
            this.BusinessKey = businessKey;
            this.AuthenticationKey = userKey;
        }

        #endregion

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
