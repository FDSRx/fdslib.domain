﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class CreateExtranetUserRequest : CreateCredentialRequest
    {
        #region Constructors

        /// <summary>
        /// Initailizes a new instance of the CreateExtranetUserRequest object.
        /// </summary>
        public CreateExtranetUserRequest() :
            base()
        {
        }

        #endregion

        /// <summary>
        /// Returns the Username property of the object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("{0}", this.Username);
        }

    }
}
