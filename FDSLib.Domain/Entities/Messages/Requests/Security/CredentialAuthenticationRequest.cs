﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{

    public class CredentialAuthenticationRequest
    {
        /// <summary>
        /// The unique key that identifies an application.
        /// </summary>
        [DefaultValue(null)]
        public string ApplicationKey { get; set; }

        /// <summary>
        /// A single or comma delimited list of business unique identifiers.
        /// </summary>
        [DefaultValue(null)]
        public string BusinessKey { get; set; }

        /// <summary>
        /// The type of business identifier that is being provided (e.g. NABP or Business Token, etc.).
        /// </summary>
        [DefaultValue(null)]
        public string BusinessKeyType { get; set; }

        /// <summary>
        /// The type of credential to authenticate (e.g. Extranet, internet, etc.).
        /// </summary>
        [DefaultValue(null)]
        public string CredentialTypeKey { get; set; }

        /// <summary>
        /// The client's username.
        /// </summary>
        [DefaultValue(null)]
        public string Username { get; set; }

        /// <summary>
        /// The passwod associated with the client's username.
        /// </summary>
        [DefaultValue(null)]
        public string Password { get; set; }

        /// <summary>
        /// A globally unique authentication token.
        /// </summary>
        [DefaultValue(null)]
        public string AuthenticationToken { get; set; }

        /// <summary>
        /// The unique identifier of the user.
        /// </summary>
        [DefaultValue(null)]
        public string CredentialKey { get; set; }

        /// <summary>
        /// A single or comma delimited list of roles.
        /// </summary>
        [DefaultValue(null)]
        public string Roles { get; set; }

        /// <summary>
        /// The numerical label assigned to the caller's device.
        /// </summary>
        [DefaultValue(null)]
        public string IpAddress { get; set; }

        /// <summary>
        /// The name of the internet browser.
        /// </summary>
        [DefaultValue(null)]
        public string BrowserName { get; set; }

        /// <summary>
        /// The version number of the browser.
        /// </summary>
        [DefaultValue(null)]
        public string BrowserVersion { get; set; }

        /// <summary>
        /// The name of the device (e.g. iphone, android, windows phone, etc.).
        /// </summary>
        [DefaultValue(null)]
        public string DeviceName { get; set; }

        /// <summary>
        /// Indicates whether the device making the request is a mobile or standard desktop device.
        /// </summary>
        [DefaultValue(null)]
        public bool IsMobile { get; set; }

        /// <summary>
        /// The software acting on behalf of the user.
        /// </summary>
        [DefaultValue(null)]
        public string UserAgent { get; set; }

        /// <summary>
        /// The name or username of the individual requesting access.
        /// </summary>
        [DefaultValue(null)]
        public string RequestedBy { get; set; }

        #region Constructors

        /// <summary>
        /// Instantiates a new instance of the CredentialAuthenticationRequest object.
        /// </summary>
        public CredentialAuthenticationRequest()
        {
        }

        /// <summary>
        /// Instantiates a new instance of the CredentialAuthenticationRequest object.
        /// </summary>
        /// <param name="businessKey">The business providing the the service to the client.</param>
        /// <param name="username">The username associated with the businesses service.</param>
        /// <param name="password">The password associated with the client's username.</param>
        public CredentialAuthenticationRequest(string businessKey, string username, string password)
        {
            this.BusinessKey = businessKey;
            this.Username = username;
            this.Password = password;
        }

        /// <summary>
        /// Instantiates a new instance of the CredentialAuthenticationRequest object.
        /// </summary>
        /// <param name="applicationKey">The application performing the service..</param>
        /// <param name="businessKey">The business providing the the service to the client.</param>
        /// <param name="username">The username associated with the businesses service.</param>
        /// <param name="password">The password associated with the client's username.</param>
        public CredentialAuthenticationRequest(string applicationKey, string businessKey, string username, string password)
        {
            this.ApplicationKey = applicationKey;
            this.BusinessKey = businessKey;
            this.Username = username;
            this.Password = password;
        }

        /// <summary>
        /// Instantiates a new instance of the CredentialAuthenticationRequest object.
        /// </summary>
        /// <param name="applicationKey">The application performing the service..</param>
        /// <param name="businessKey">The business providing the the service to the client.</param>
        /// <param name="credentialTypeKey">The type of credential being evaluated (e.g. Extranet, internet, etc.).</param>
        /// <param name="username">The username associated with the businesses service.</param>
        /// <param name="password">The password associated with the client's username.</param>
        public CredentialAuthenticationRequest(string applicationKey, string businessKey, string credentialTypeKey, string username, string password)
        {
            this.ApplicationKey = applicationKey;
            this.BusinessKey = businessKey;
            this.CredentialTypeKey = credentialTypeKey;
            this.Username = username;
            this.Password = password;
        }

        #endregion

        /// <summary>
        /// Returns the username property.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("Username: {0} | AuthenticationKey: {1} | CredentialKey: {2}", this.Username,
                this.AuthenticationToken, this.CredentialKey);
        }



    }
}
