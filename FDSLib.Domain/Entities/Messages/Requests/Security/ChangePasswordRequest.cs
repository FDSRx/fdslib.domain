﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class ChangePasswordRequest
    {
        /// <summary>
        /// A single or comma delimited list of applications to apply to the user.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string ApplicationKey { get; set; }

        /// <summary>
        /// The unique identifier of the business to apply to the user.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string BusinessKey { get; set; }

        /// <summary>
        /// The type of business key that is being requested (i.e. NABP, BusinessID, Source Store ID, etc.).
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string BusinessKeyType { get; set; }

        /// <summary>
        /// A unique name that identifies the user.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Username { get; set; }

        /// <summary>
        /// An alpha-numeric string that accompanies the username.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Password { get; set; }

        /// <summary>
        /// The type of credential to authenticate (e.g. Extranet, internet, etc.).
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string CredentialTypeKey { get; set; }

        /// <summary>
        /// A globally unique system defined identifier of a user.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string CredentialKey { get; set; }

        /// <summary>
        /// An alpha-numeric string that should match the password property.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// A new alpha-numeric string that acompanies the username.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string NewPassword { get; set; }

        /// <summary>
        /// An alpha-numeric string that should match the new password property.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string ConfirmNewPassword { get; set; }

        /// <summary>
        /// The date/time the password is set to expire.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string DatePasswordExpires { get; set; }

        /// <summary>
        /// The name or username of the individual that is performing the change password.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string ModifiedBy { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ChangePasswordRequest object.
        /// </summary>
        public ChangePasswordRequest()
        {
        }

        #endregion

        /// <summary>
        /// Returns the Name or CredentialKey property (depending on which field is populated).
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return !String.IsNullOrEmpty(this.Username) ? this.Username : this.CredentialKey;
        }

    }
}
