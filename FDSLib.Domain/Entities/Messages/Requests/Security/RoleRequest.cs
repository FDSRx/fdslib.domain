﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class RoleRequest : BasicRequest
    {
        /// <summary>
        /// A single or comma delimited list of unique role identifiers.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string RoleKey { get; set; }

        #region Constructors

        /// <summary>
        /// Instantiates a new instance of the RoleRequest object.
        /// </summary>
        public RoleRequest()
        {
        }

        /// <summary>
        /// Instantiates a new instance of the RoleRequest object.
        /// </summary>
        /// <param name="applicationKey">A unique key that identifies an application.</param>
        /// <param name="businessKey">A single or comma delimited list of business unique identifiers.</param>
        public RoleRequest(string applicationKey, string businessKey)
        {
            this.ApplicationKey = applicationKey;
            this.BusinessKey = businessKey;
        }

        #endregion

        /// <summary>
        /// Returns the System.String representation of the request.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
