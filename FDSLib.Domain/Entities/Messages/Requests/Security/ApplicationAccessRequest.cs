﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FDSLib.Domain.Enumerations;

namespace FDSLib.Domain.Entities.Messages
{
    public class ApplicationAccessRequest
    {
        /// <summary>
        /// The unique code that identifies an application.
        /// </summary>
        public Domain.Enumerations.Application Application {get; set;}

        /// <summary>
        /// A single or comma delimited list of pharmacy store identifiers.
        /// </summary>
        public string Nabp { get; set; }

        /// <summary>
        /// The unique code that identifies the type of access inspection to be performed (e.g. host inspection, environment variable inspection, ect.).
        /// </summary>
        public AccessInspectionType? AccessInspectionType { get; set; }

        /// <summary>
        /// The unique code that identifies the type of filter being inspected (e.g. Ip address, domain name, etc.).
        /// </summary>
        public AccessFilterType? AccessFilterType { get; set; }

        /// <summary>
        /// The value of the filter item.
        /// </summary>
        public string Filter { get; set; }

        /// <summary>
        /// The unique code that identifies the type of access directive allowed (i.e. allow, deny, etc.).
        /// </summary>
        public AccessDirective? AccessDirective { get; set; } 

        /// <summary>
        /// Initializes a new instance of the ApplicationAccessRequest object.
        /// </summary>
        /// <param name="application">The application that is to be requested.</param>
        /// <param name="nabp">The unique store identifier accessing the application.</param>
        /// <param name="filter">The filter that needs to be verified for access.</param>
        public ApplicationAccessRequest(Domain.Enumerations.Application application, string nabp, string filter)
        {
            if (String.IsNullOrEmpty(nabp))
            {
                throw new ArgumentNullException("nabp");
            }

            if (String.IsNullOrEmpty(filter))
            {
                throw new ArgumentNullException("filter");
            }

            this.Application = application;
            this.Nabp = nabp;
            this.Filter = filter;
        }

        /// <summary>
        /// Returns the access request information.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("Store, {0}, for application, {1}, is requesting access for {2}.", this.Nabp, this.Application.ToString(), this.Filter);
        }
    }
}