﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class ConfigurationRequest : BasicRequest
    {

        /// <summary>
        /// The name of the key.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Key { get; set; }

        /// <summary>
        /// The value that is assigned to the key.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Value { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ConfigurationRequest object.
        /// </summary>
        public ConfigurationRequest()
        {
        }

        #endregion

        /// <summary>
        /// Returns the String representation of the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("Application: {0} | Business: {1} | Key: {2}",
                this.ApplicationKey,
                this.BusinessKey,
                this.Key
                );
        }


    }
}
