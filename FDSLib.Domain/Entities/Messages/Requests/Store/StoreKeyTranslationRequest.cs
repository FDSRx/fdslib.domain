﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    public class StoreKeyTranslationRequest : RequestBase
    {
        /// <summary>
        /// The type of store key (e.g. NABP, Source system key, system key, etc.).
        /// </summary>
        public string KeyType { get; set; }

        /// <summary>
        /// Initializes a new instance of the StoreKeyTranslationRequest object.
        /// </summary>
        public StoreKeyTranslationRequest()
        {
        }

        /// <summary>
        /// Initializes a new instance of the StoreKeyTranslationRequest object.
        /// </summary>
        /// <param name="key">The unique store identifier.</param>
        /// <param name="type">The type of unique identifier.</param>
        public StoreKeyTranslationRequest(string key, string type)
        {
            this.BusinessKey = key;
            this.KeyType = type;
        }

        /// <summary>
        /// Returns the StoreKey property.
        /// </summary>
        /// <returns>System.String.</returns>
        public override string ToString()
        {
            return String.Format("{0}: {1}", this.KeyType, this.BusinessKey);
        }
    }
}
