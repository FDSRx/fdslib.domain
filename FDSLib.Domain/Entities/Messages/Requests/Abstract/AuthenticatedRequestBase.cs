﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using FDSLib.Common.Extensions;

namespace FDSLib.Domain.Entities.Messages
{
    public abstract class AuthenticatedRequestBase : RequestBase
    {
        /// <summary>
        /// The unique identifier of the credential.
        /// </summary>
        [DefaultValue(null)]
        public string CredentialKey { get; set; }

        /// <summary>
        /// The type of credential key that is being provided.
        /// </summary>
        [DefaultValue(null)]
        public string CredentialTypeKey { get; set; }

        /// <summary>
        /// A globally unique authentication token.
        /// </summary>
        [DefaultValue(null)]
        public string AuthenticationKey { get; set; }

        /// <summary>
        /// A unique name that identifies a user.
        /// </summary>
        [DefaultValue(null)]
        public string Username { get; set; }

        /// <summary>
        /// The user's password.
        /// </summary>
        [DefaultValue(null)]
        public string Password { get; set; }

        #region Constructors

        /// <summary>
        /// Instantiates a new instance of the AuthenticatedRequestBase object.
        /// </summary>
        protected AuthenticatedRequestBase()
        {
        }

        /// <summary>
        /// Instantiates a new instance of the AuthenticatedRequestBase object.
        /// </summary>
        /// <param name="username">A unique name that identifies a user.</param>
        /// <param name="password">The password that allows access.</param>
        protected AuthenticatedRequestBase(string username, string password)
        {
            this.Username = username;
            this.Password = password;
        }

        /// <summary>
        /// Instantiates a new instance of the AuthenticatedRequestBase object.
        /// </summary>
        /// <param name="applicationKey">Unique key used to identify an application.</param>
        /// <param name="businessKey">Unique business identifier.</param>
        /// <param name="authenticationKey">The unique security token distributed to a client.</param>
        protected AuthenticatedRequestBase(string applicationKey, string businessKey, string authenticationKey)
        {
            this.ApplicationKey = applicationKey;
            this.BusinessKey = businessKey;
            this.AuthenticationKey = authenticationKey;
        }

        #endregion

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("Application: {0} | Business: {1} | Credential: {2} | User: {3} | Password: {4}",
                this.ApplicationKey,
                this.BusinessKey,
                this.CredentialKey.IfNullOrEmpty(this.AuthenticationKey),
                this.Username,
                this.Password);
        }


    }
}
