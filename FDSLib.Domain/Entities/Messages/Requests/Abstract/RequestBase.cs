﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace FDSLib.Domain.Entities.Messages
{
    public abstract class RequestBase
    {
        /// <summary>
        /// The unique key that identifies an application.
        /// </summary>
        [DefaultValue(null)]
        public string ApplicationKey { get; set; }

        /// <summary>
        /// A single or comma delimited list of business unique identifiers.
        /// </summary>
        [DefaultValue(null)]
        public string BusinessKey { get; set; }

        /// <summary>
        /// The type of business key(s) provided (e.g. Nabp, Store Id, etc.).
        /// </summary>
        [DefaultValue(null)]
        public string BusinessKeyType { get; set; }

        /// <summary>
        /// The username or name of the individual making the request.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string RequesterKey { get; set; }

        /// <summary>
        /// A culture specific identifier that indicates the culture in which the data should be returned.
        /// </summary>
        [DefaultValue(null)]
        public string LanguageKey { get; set; }

        /// <summary>
        /// A set of start and end dates used to query the data (where applicable).
        /// </summary>
        [DefaultValue(null)]
        public DateRange DataDateRange { get; set; }

        /// <summary>
        /// The number of records to skip.
        /// </summary>
        [DefaultValue(null)]
        public long? Skip { get; set; }

        /// <summary>
        /// The number of records to return.
        /// </summary>
        [DefaultValue(null)]
        public long? Take { get; set; }

        /// <summary>
        /// A string collection of filter parameters.
        /// </summary>
        [DefaultValue(null)]
        public string Filter { get; set; }

        /// <summary>
        /// A collection of sort parameters.
        /// </summary>
        [DefaultValue(null)]
        public string Sort { get; set; }

        #region Constructors

        /// <summary>
        /// Instantiates a new instance of the RequestBase object.
        /// </summary>
        protected RequestBase()
        {
            this.DataDateRange = new DateRange();
        }

        /// <summary>
        /// Instantiates a new instance of the RequestBase object.
        /// </summary>
        /// <param name="businessKey">A single or comma delimited list of business unique identifiers.</param>
        protected RequestBase(string businessKey) :
            this()
        {
            this.BusinessKey = businessKey;
        }

        /// <summary>
        /// Instantiates a new instance of the RequestBase object.
        /// </summary>
        /// <param name="applicationKey">A unique key that identifies an application.</param>
        /// <param name="businessKey">A single or comma delimited list of business unique identifiers.</param>
        protected RequestBase(string applicationKey, string businessKey) :
            this(businessKey)
        {
            this.ApplicationKey = applicationKey;
        }

        /// <summary>
        /// Instantiates a new instance of the RequestBase object.
        /// </summary>
        /// <param name="applicationKey">A unique key that identifies an application.</param>
        /// <param name="businessKey">A single or comma delimited list of business unique identifiers.</param>
        /// <param name="range">A set of start and end dates used to query the data.</param>
        protected RequestBase(string applicationKey, string businessKey, DateRange range) :
            this(applicationKey, businessKey)
        {
            this.DataDateRange = range;
        }

        #endregion

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("Application: {0} | Business: {1}",
                this.ApplicationKey,
                this.BusinessKey);
        }
    }
}