﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Common.Helpers;

namespace FDSLib.Domain.Entities.Messages
{
    public static class ConfigurationResponseExtensions
    {
        /// <summary>
        /// Returns the value associated with the object.
        /// </summary>
        /// <param name="obj">ConfigurationResponse object.</param>
        public static string GetValue(this ConfigurationResponse obj)
        {
            return obj.Value;
        }

        /// <summary>
        /// Returns the value associated with the object. Returns the provided default value or null if an error is encountered.
        /// </summary>
        /// <param name="obj">ConfigurationResponse object.</param>
        /// <param name="defaultValue">The default value to return if a null is encountered.</param>
        public static string GetValue(this ConfigurationResponse obj, string defaultValue)
        {
            return obj.Value ?? defaultValue;
        }

        /// <summary>
        /// Returns the value associated with the object. Returns the default value of null if an error is encountered.
        /// </summary>
        /// <param name="obj">ConfigurationResponse object.</param>
        /// <returns></returns>
        public static string GetValueOrDefault(this ConfigurationResponse obj)
        {
            string value = null;

            try
            {
                value = obj.Value;
            }
            catch
            {
            }

            return value;
        }

        /// <summary>
        /// Returns the value associated with the object. Returns the provided default value or null if an error is encountered.
        /// </summary>
        /// <param name="obj">ConfigurationResponse object.</param>
        /// <param name="defaultValue">The default value to return if a null or error is encountered.</param>
        /// <returns></returns>
        public static string GetValueOrDefault(this ConfigurationResponse obj, string defaultValue)
        {
            string value = null;

            try
            {
                value = obj.Value;
            }
            catch
            {
                value = defaultValue;
            }

            return value ?? defaultValue;
        }

        /// <summary>
        /// Returns the value associated with the object.
        /// </summary>
        /// <param name="obj">ConfigurationResponse object.</param>
        public static T GetValue<T>(this ConfigurationResponse obj)
        {
            var val = obj.Value;

            return ObjectHelper.ConvertToType<T>(val);
        }

        /// <summary>
        /// Returns the value associated with the object.
        /// </summary>
        /// <param name="obj">ConfigurationResponse object.</param>
        /// <param name="defaultValue">The default value to return if a null is encountered.</param>
        /// <returns></returns>
        public static T GetValue<T>(this ConfigurationResponse obj, string defaultValue)
        {
            var val = obj.Value;

            return ObjectHelper.ConvertToType<T>(val ?? defaultValue);
        }

        /// <summary>
        /// Returns the value associated with the object. Returns the default value for TyoeOF(T) if an error is encountered.
        /// </summary>
        /// <param name="obj">ConfigurationResponse object.</param>
        public static T GetValueOrDefault<T>(this ConfigurationResponse obj)
        {
            try
            {
                var val = obj.Value;

                return ObjectHelper.ConvertToType<T>(val);
            }
            catch
            {

            }

            return default(T);
        }

        /// <summary>
        /// Returns the value associated with the object. Returns the provided default value or 
        /// the default value for TyoeOF(T) if an error is encountered.
        /// </summary>
        /// <param name="obj">ConfigurationResponse object.</param>
        /// <param name="defaultValue">The default value for TypeOf(T) to return if a null or error is encountered.</param>
        public static T GetValueOrDefault<T>(this ConfigurationResponse obj, T defaultValue)
        {
            try
            {
                var val = obj.Value;

                return ObjectHelper.ConvertToType<T>(val);
            }
            catch
            {
                return defaultValue;
            }

            //return default(T);
        }

        /// <summary>
        /// Returns the value associated with the object. Returns null if an error is encountered.
        /// </summary>
        /// <param name="obj">ConfigurationResponse object.</param>
        public static T? GetValueOrNull<T>(this ConfigurationResponse obj) where T : struct
        {
            try
            {
                return (obj.Value == null) ? (T?)null : ObjectHelper.ConvertToType<T>(obj.Value);
            }
            catch
            {

            }

            return null;
        }

        /// <summary>
        /// Returns the value associated with the object. Returns the provided default value or null if an error is encountered.
        /// </summary>
        /// <param name="obj">ConfigurationResponse object.</param>
        /// <param name="defaultValue">The default value for TypeOf(T) to return if a null or error is encountered.</param>
        public static T? GetValueOrNull<T>(this ConfigurationResponse obj, T? defaultValue) where T : struct
        {

            try
            {
                return (obj.Value == null) ? defaultValue : ObjectHelper.ConvertToType<T>(obj.Value);
            }
            catch
            {

            }

            return defaultValue;
        }
    }
}
