﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class ConfigurationResponse : SingleValueResponse
    {

        /// <summary>
        /// The key that identifies the value.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Key { get; set; }

        /// <summary>
        /// Gets/Sets the type of value that is returned (e.g. int, string, etc.).
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string ValueTypeOf { get; set; }

        /// <summary>
        /// Returns a description of the key/value pair.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Description { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ConfigurationResponse object.
        /// </summary>
        public ConfigurationResponse()
        {
        }

        #endregion

        /// <summary>
        /// Returns the String representation of the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Value;
        }
    }
}
