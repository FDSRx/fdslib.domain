﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public abstract class ResponseBase : BusinessEntity<string>
    {
        /// <summary>
        /// Indicates if the request successfully provided a response.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public bool IsSuccess { get; set; }

        /// <summary>
        /// A friendly message that describes the response data or an issue with the request.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string Message { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ResponseBase object.
        /// </summary>
        protected ResponseBase()
        {
        }
        
        /// <summary>
        /// Initializes a new instance of the ResponseBase object.
        /// </summary>
        /// <param name="isSuccess">Indicates whether the response was successful or a failure.</param>
        /// <param name="message">A friendly message that describes the outcome of the request.</param>
        protected ResponseBase(bool isSuccess, string message)
        {
            this.IsSuccess = isSuccess;
            this.Message = message;
        }

        #endregion

        /// <summary>
        /// Returns the message property of the response.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Message;
        }
    }
}
