﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    public class CreateUserResponse : BasicResponse
    {
        /// <summary>
        /// Indicates whether the user has already been created.
        /// </summary>
        public bool IsPreviouslyCreated { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the CreateUserResponse object.
        /// </summary>
        public CreateUserResponse()
        {
        }

        #endregion

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
