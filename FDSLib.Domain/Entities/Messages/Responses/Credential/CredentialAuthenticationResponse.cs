﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class CredentialAuthenticationResponse : TokenAuthenticationResponse
    {
        /// <summary>
        /// Indicates whether the set of credentials is valid or invalid.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public bool IsValid { get; set; }

        /// <summary>
        /// Indicates whether the set of credentials is locked out.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public bool IsLockedOut { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the CredentialAuthenticationResponse object.
        /// </summary>
        public CredentialAuthenticationResponse()
        {
        }

        #endregion

        public override string ToString()
        {
            return base.ToString();
        }

    }
}