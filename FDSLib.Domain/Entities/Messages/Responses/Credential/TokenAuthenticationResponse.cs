﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class TokenAuthenticationResponse : ResponseBase
    {
        private DateTime? _dateExpires = null;

        /// <summary>
        /// A globally unique authentication token.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string Token { get; set; }

        /// <summary>
        /// Indicates whether the token is expired or still active.  True if active; otherwise, false if expired.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public bool IsExpired { get; set; }

        /// <summary>
        /// The date the token has or will expire.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        [XmlIgnore]
        public DateTime? DateExpires
        {
            get { return this._dateExpires; }

            set { this._dateExpires = value; }
        }

        /// <summary>
        /// The System.String representation of the date/time the token has or will expire.
        /// </summary>
        [XmlAttribute("DateExpires")]
        public string DateExpiresString
        {
            get
            {
                return this._dateExpires.HasValue
                    ? XmlConvert.ToString(this._dateExpires.Value, XmlDateTimeSerializationMode.Unspecified)
                    : string.Empty;
            }
            set
            {
                this._dateExpires = !string.IsNullOrEmpty(value)
                    ? XmlConvert.ToDateTime(value, XmlDateTimeSerializationMode.Unspecified)
                    : (DateTime?) null;
            }
        }

        #region Construtctors

        /// <summary>
        /// Initializes a new instance of the TokenAuthenticationResponse object.
        /// </summary>
        public TokenAuthenticationResponse()
        {
        }

        #endregion

        /// <summary>
        /// Returns the Token property of the object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Token;
        }

    }

}

