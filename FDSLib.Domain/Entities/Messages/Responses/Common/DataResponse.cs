﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    /// <summary>
    /// General response object that contains information regarding the success of a request.
    /// </summary>
    public class DataResponse : ResponseBase
    {
        /// <summary>
        /// Contains information regarding an unsuccessful request/response.
        /// </summary>
        public Error Error { get; set; }

        /// <summary>
        /// A generic key that correlates to the context of a request (e.g. User token, store token, etc.).
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// The status code of a resposne.
        /// </summary>
        public string StatusCode { get; set; }

        /// <summary>
        /// The name of the status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Data value contained within the response (if applicable).
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// Initializes a new instance of the DataResponse object.
        /// </summary>
        public DataResponse()
        {
            this.Error = new Error();
        }

        /// <summary>
        /// Initializes a new instance of the DataResponse object.
        /// </summary>
        /// <param name="isSuccess">Indicates whether the response was successful or a failure.</param>
        /// <param name="message">A friendly message that describes the outcome of the request.</param>
        public DataResponse(bool isSuccess, string message) :
            base(isSuccess, message)
        {
            this.Error = new Error();
        }

        /// <summary>
        /// Returns the message property of the response object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Message;
        }
    }
}
