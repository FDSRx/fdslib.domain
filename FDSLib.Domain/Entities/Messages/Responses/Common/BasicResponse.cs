﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class BasicResponse : ResponseBase
    {
        /// <summary>
        /// A value or set of values that is returned from the request.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string Value { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the BasicResponse object.
        /// </summary>
        public BasicResponse()
        {
        }

        #endregion

        /// <summary>
        /// Returns the Value property of the object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Value;
        }
    }
}
