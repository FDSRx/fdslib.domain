﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    /// <summary>
    /// An object used to return single value responses.
    /// </summary>
    [DataContract]
    public class SingleValueResponse : ResponseBase
    {
        /// <summary>
        /// The value.
        /// </summary>
        [DataMember]
        public string Value { get; set; }

        /// <summary>
        /// Initializes a new instance of the SingleValueResponse object.
        /// </summary>
        public SingleValueResponse()
        {
        }

        /// <summary>
        /// Initializes a new instance of the SingleValueResponse object.
        /// </summary>
        /// <param name="value">The value.</param>
        public SingleValueResponse(string value)
        {
            this.Value = value;
        }

        /// <summary>
        /// Initializes a new instance of the SingleValueResponse object.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="isSuccess">Indicates whether the reponse was as success or failure.</param>
        public SingleValueResponse(string value, bool isSuccess)
        {
            this.Value = value;
            this.IsSuccess = isSuccess;
        }

        /// <summary>
        /// Initializes a new instance of the SingleValueResponse object.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="isSuccess">Indicates whether the reponse was as success or failure.</param>
        /// <param name="message">Response message (if applicable).</param>
        public SingleValueResponse(string value, bool isSuccess, string message)
        {
            this.Value = value;
            this.IsSuccess = isSuccess;
            this.Message = message;
        }

        /// <summary>
        /// Returns the Value property.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Value;
        }
    }
}
