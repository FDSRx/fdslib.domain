﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FDSLib.Domain.Entities.Messages
{
    public class ApplicationAccessResponse : ResponseBase
    {
        /// <summary>
        /// True if access is allowed; Otherwise, false if access is denied.
        /// </summary>
        public bool IsAccessAllowed { get; set; }

        /// <summary>
        /// Returns the message.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("{0}", this.Message);
        }
       
    }
}