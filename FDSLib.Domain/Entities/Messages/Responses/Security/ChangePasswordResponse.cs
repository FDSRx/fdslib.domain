﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities.Messages
{
    [DataContract]
    public class ChangePasswordResponse : ResponseBase
    {
        /// <summary>
        /// Indicates whether the set of credentials is valid or invalid.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsValid { get; set; }

        /// <summary>
        /// Indicates whether the set of credentials is locked out.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsLockedOut { get; set; }

        /// <summary>
        /// Indicates whether the account has been authorized.
        /// </summary>
        //[DefaultValue(false)]
        //[DataMember(EmitDefaultValue = false)]
        //public bool IsApproved { get; set; }

        /// <summary>
        /// Indicates whether the account has been disabled.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public bool IsDisabled { get; set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ChangePasswordResponse object.
        /// </summary>
        public ChangePasswordResponse()
        {
        }

        #endregion

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
