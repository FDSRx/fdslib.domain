﻿
using System.Collections.Generic;
using System.Linq;
using FDSLib.Common.Enumerations;
using FDSLib.Domain.Enumerations;

namespace FDSLib.Domain.Entities
{
    public class Service : BaseReferenceType
    {
        /// <summary>
        /// The name of the image file that is associated with the service.
        /// </summary>
        public string ImageFileName { get; set; }

        /// <summary>
        /// A unique key that links the service to the user.
        /// </summary>
        public string AccountKey { get; set; }

        /// <summary>
        /// The current state of the service as it pertains to the user (e.g. the user is active within the service, the user has 
        /// terminated the service, etc.).
        /// </summary>
        public ConnectionStatus ConnectionStatus { get; set; }

        /// <summary>
        /// Creates a new Service object.
        /// </summary>
        public Service() { this.ConnectionStatus = new ConnectionStatus(); }

        /// <summary>
        /// Creates a new Service object.
        /// </summary>
        /// <param name="name">Name of service.</param>
        public Service(string name) : base(null, null, name, null) { this.ConnectionStatus = new ConnectionStatus(); }

        /// <summary>
        /// Creates a new Service object.
        /// </summary>
        /// <param name="id">Service unique identifier.</param>
        /// <param name="code">Service unique code.</param>
        /// <param name="name">Name of service.</param>
        public Service(string id, string code, string name) : base(id, code, name, null) { this.ConnectionStatus = new ConnectionStatus(); }

        /// <summary>
        /// Creates a new Service object.
        /// </summary>
        /// <param name="code">Service unique code.</param>
        /// <param name="name">Name of service.</param>
        public Service(string code, string name) : base(null, code, name, null) { this.ConnectionStatus = new ConnectionStatus(); }

        /// <summary>
        /// Creates a new Service object.
        /// </summary>
        /// <param name="id">Service unique identifier.</param>
        /// <param name="code">Service unique code.</param>
        /// <param name="name">Name of service.</param>
        /// <param name="description">Description of service.</param>
        public Service(string id, string code, string name, string description) :
            base(id, code, name, description) { this.ConnectionStatus = new ConnectionStatus(); }

        /// <summary>
        /// Indicates whether the list of services contains the provided service.
        /// </summary>
        /// <param name="serviceName">Service to lookup.</param>
        /// <param name="services">An enumerable list of services.</param>
        /// <returns></returns>
        public static bool HasService(ServiceName serviceName, IEnumerable<Domain.Entities.Service> services)
        {
            bool hasService = false;

            if (services != null && services.Count() > 0)
            {
                foreach (Domain.Entities.Service s in services)
                {
                    if (serviceName.GetCode().Equals(s.Code))
                    {
                        hasService = true;
                    }
                }
            }

            return hasService;
        }

        /// <summary>
        /// Retrieves the provided service from the list of services.
        /// </summary>
        /// <param name="serviceName">Service to lookup.</param>
        /// <param name="services">An enumerable list of services.</param>
        /// <returns></returns>
        public static Domain.Entities.Service GetService(ServiceName serviceName, IEnumerable<Domain.Entities.Service> services)
        {
            var service = new Service();

            if (services != null && services.Count() > 0)
            {
                foreach (Service s in services)
                {
                    if (serviceName.GetCode().Equals(s.Code))
                    {
                        service = s;
                    }
                }
            }

            return service;
        }

        /// <summary>
        /// Returns the name of the service.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}