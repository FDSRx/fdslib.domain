﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    [KnownType(typeof(BaseReferenceType))]
    [KnownType(typeof(ReferenceTypeMetaData))]
    public class TimeZone : BaseReferenceType
    {
        /// <summary>
        /// The amount of time that is to be offset.
        /// </summary>
        [DefaultValue(0)]
        [DataMember(EmitDefaultValue=false)]
        public int OffSet { get; set; }

        /// <summary>
        /// Gets or sets the time zone abbreviation (includes daylight savings time when applicable).
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Abbreviation { get; set; }

        /// <summary>
        /// Gets or sets the prefix of the time zone (e.g. CST = C, MST = M, etc.).
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string Prefix { get; set; }

        /// <summary>
        /// Creates a new TimeZone object.
        /// </summary>
        public TimeZone() { }

        /// <summary>
        /// Creates a new TimeZone object.
        /// </summary>
        /// <param name="name">Name of TimeZone.</param>
        public TimeZone(string name) : base(null, null, name, null) { }

        /// <summary>
        /// Creates a new TimeZone object.
        /// </summary>
        /// <param name="id">TimeZone unique identifier.</param>
        /// <param name="code">TimeZone unique code.</param>
        /// <param name="name">Name of TimeZone.</param>
        public TimeZone(string id, string code, string name) : base(id, code, name, null) { }

        /// <summary>
        /// Creates a new TimeZone object.
        /// </summary>
        /// <param name="code">TimeZone unique code.</param>
        /// <param name="name">Name of TimeZone.</param>
        public TimeZone(string code, string name) : base(null, code, name, null) { }

        /// <summary>
        /// Creates a new TimeZone object.
        /// </summary>
        /// <param name="id">TimeZone unique identifier.</param>
        /// <param name="code">TimeZone unique code.</param>
        /// <param name="name">Name of TimeZone.</param>
        /// <param name="description">Description of TimeZone.</param>
        public TimeZone(string id, string code, string name, string description) :
            base(id, code, name, description) { }


        /// <summary>
        /// Returns the "Name" property of the object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }

    }
}
