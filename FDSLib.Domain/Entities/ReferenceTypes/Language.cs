﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    [KnownType(typeof(BaseReferenceType))]
    [KnownType(typeof(ReferenceTypeMetaData))]
    public class Language : BaseReferenceType
    {
        /// <summary>
        /// Creates a new language object.
        /// </summary>
        public Language() { }

        /// <summary>
        /// Creates a new language object.
        /// </summary>
        /// <param name="name">Name of service.</param>
        public Language(string name) : base(null, null, name, null) { }

        /// <summary>
        /// Creates a new language object.
        /// </summary>
        /// <param name="id">Language unique identifier.</param>
        /// <param name="code">Language unique code.</param>
        /// <param name="name">Name of language.</param>
        public Language(string id, string code, string name) : base(id, code, name, null) { }

        /// <summary>
        /// Creates a new language object.
        /// </summary>
        /// <param name="code">Language unique code.</param>
        /// <param name="name">Name of language.</param>
        public Language(string code, string name) : base(null, code, name, null) { }

        /// <summary>
        /// Creates a new language object.
        /// </summary>
        /// <param name="id">Language unique identifier.</param>
        /// <param name="code">Language unique code.</param>
        /// <param name="name">Name of language.</param>
        /// <param name="description">Description of language.</param>
        public Language(string id, string code, string name, string description) :
            base(id, code, name, description) { }


        /// <summary>
        /// Returns the name of the language.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}