﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    [KnownType(typeof(BaseReferenceType))]
    [KnownType(typeof(ReferenceTypeMetaData))]
    public class ContactType : BaseReferenceType
    {

        /// <summary>
        /// Creates a new ContactType object.
        /// </summary>
        public ContactType() { }

        /// <summary>
        /// Creates a new ContactType object.
        /// </summary>
        /// <param name="name">Name of ContactType.</param>
        public ContactType(string name) : base(null, null, name, null) { }

        /// <summary>
        /// Creates a new ContactType object.
        /// </summary>
        /// <param name="id">ContactType unique identifier.</param>
        /// <param name="code">CodeQualifier unique code.</param>
        /// <param name="name">Name of ContactType.</param>
        public ContactType(string id, string code, string name) : base(id, code, name, null) { }

        /// <summary>
        /// Creates a new ContactType object.
        /// </summary>
        /// <param name="code">ContactType unique code.</param>
        /// <param name="name">Name of ContactType.</param>
        public ContactType(string code, string name) : base(null, code, name, null) { }

        /// <summary>
        /// Creates a new ContactType object.
        /// </summary>
        /// <param name="id">ContactType unique identifier.</param>
        /// <param name="code">ContactType unique code.</param>
        /// <param name="name">Name of ContactType.</param>
        /// <param name="description">Description of ContactType.</param>
        public ContactType(string id, string code, string name, string description) :
            base(id, code, name, description) { }


        /// <summary>
        /// Returns the "Name" property of the CodeQualifier.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }

    }
}
