﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities
{
    public class Role : BaseReferenceType
    {
        #region Constructors

        /// <summary>
        /// Creates a new Role object.
        /// </summary>
        public Role() { }

        /// <summary>
        /// Creates a new Role object.
        /// </summary>
        /// <param name="name">Name of Role.</param>
        public Role(string name) : base(null, null, name, null) { }

        /// <summary>
        /// Creates a new Role object.
        /// </summary>
        /// <param name="id">Role unique identifier.</param>
        /// <param name="code">Role unique code.</param>
        /// <param name="name">Name of Role.</param>
        public Role(string id, string code, string name) : base(id, code, name, null) { }

        /// <summary>
        /// Creates a new Role object.
        /// </summary>
        /// <param name="code">Role unique code.</param>
        /// <param name="name">Name of Role.</param>
        public Role(string code, string name) : base(null, code, name, null) { }

        /// <summary>
        /// Creates a new Role object.
        /// </summary>
        /// <param name="id">Role unique identifier.</param>
        /// <param name="code">Role unique code.</param>
        /// <param name="name">Name of Role.</param>
        /// <param name="description">Description of Role.</param>
        public Role(string id, string code, string name, string description) :
            base(id, code, name, description) { }

        #endregion

        /// <summary>
        /// Creates a new List(Of Role) object.
        /// </summary>
        /// <param name="roles">An array of roles.</param>
        /// <returns></returns>
        public static List<Role> CreateRoles(string[] roles)
        {
            if (roles == null)
            {
                throw new ArgumentNullException("roles");
            }

            var list = new List<Role>();

            foreach (var role in roles)
            {
                if (!String.IsNullOrWhiteSpace(role))
                {
                    list.Add(new Role(role.Trim()));
                }
            }

            return list;
        }

        /// <summary>
        /// Creates a new List(Of Role) object.
        /// </summary>
        /// <param name="roles">A single or comma delimited string of roles.</param>
        /// <returns></returns>
        public static List<Role> CreateRoles(string roles)
        {
            if (String.IsNullOrEmpty(roles))
            {
                throw new ArgumentNullException("roles");
            }

            var arr = roles.Split(",".ToCharArray());

            return CreateRoles(arr);
        }

        /// <summary>
        /// Converts a single or comma delimited list of roles into an enumerable System.Collections.Generic.List(Of Role) object.
        /// </summary>
        /// <param name="roles">A single or comma delimited list of roles.</param>
        /// <returns></returns>
        public static List<Role> CreateRolesOrDefault(string roles)
        {
            try
            {
                return CreateRoles(roles);
            }
            catch
            {
                return new List<Role>();
            }
        }

        /// <summary>
        /// Parses a single or comma delimited string of roles to an array.
        /// </summary>
        /// <param name="roles">A single or comma delimited string of roles.</param>
        /// <returns></returns>
        public static string[] ParseRolesToArray(string roles)
        {
            return ParseRolesToArray(roles, ",");
        }

        /// <summary>
        /// Parses a single or delimited string of roles to an array.
        /// </summary>
        /// <param name="roles">A single or delimited string of roles.</param>
        /// <param name="delimiter">The delimited that is used to separate the list of roles (e.g. ",", "|", etc.).</param>
        /// <returns></returns>
        public static string[] ParseRolesToArray(string roles, string delimiter)
        {
            if (String.IsNullOrEmpty(roles))
            {
                throw new ArgumentNullException("roles");
            }

            if (String.IsNullOrEmpty(delimiter))
            {
                throw new ArgumentNullException("delimiter");
            }

            var arr = roles.Split(delimiter.ToCharArray());

            return arr;
        }

        /// <summary>
        /// Parses a single or comma delimited string of roles to a list.
        /// </summary>
        /// <param name="roles">A single or comma delimited string of roles.</param>
        /// <returns></returns>
        public static List<Role> ParseRolesToList(string roles)
        {
            return ParseRolesToList(roles, ",");
        }

        /// <summary>
        /// Parses a single or delimited string of roles to a list.
        /// </summary>
        /// <param name="roles">A single or delimited string of roles.</param>
        /// <param name="delimiter">The delimited that is used to separate the list of roles (e.g. ",", "|", etc.).</param>
        /// <returns></returns>
        public static List<Role> ParseRolesToList(string roles, string delimiter)
        {
            if (String.IsNullOrEmpty(roles))
            {
                throw new ArgumentNullException("roles");
            }

            if (String.IsNullOrEmpty(delimiter))
            {
                throw new ArgumentNullException("delimiter");
            }

            var list = new List<Role>();

            var arr = roles.Split(delimiter.ToCharArray());

            foreach (var role in arr)
            {
                list.Add(new Role()
                         {
                             Id = role,
                             Code = role,
                             Name = role
                         });
            }

            return list;
        }

        /// <summary>
        /// Returns the "Name" property of the Role.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }

    }
}
