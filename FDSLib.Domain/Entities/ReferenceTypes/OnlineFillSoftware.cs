﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    [KnownType(typeof(BaseReferenceType))]
    [KnownType(typeof(ReferenceTypeMetaData))]
    public class OnlineFillSoftware : BaseReferenceType, IProviderUser
    {
        /// <summary>
        /// Gets or sets the username associated with the provider.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the unencrypted password associated with the provider.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string PasswordUnencrypted { get; set; }

        /// <summary>
        /// Gets or sets the encrypted password associated with the provider.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string PasswordEncrypted { get; set; }

        #region Constructors

        /// <summary>
        /// Initalizes a new isntance of the OnlineRefillSoftware object.
        /// </summary>
        public OnlineFillSoftware() { }

        /// <summary>
        /// Initalizes a new isntance of the OnlineRefillSoftware object.
        /// </summary>
        /// <param name="name">Name of OnlineRefillSoftware.</param>
        public OnlineFillSoftware(string name) : base(null, null, name, null) { }

        /// <summary>
        /// Initalizes a new isntance of the OnlineRefillSoftware object.
        /// </summary>
        /// <param name="id">OnlineRefillSoftware unique identifier.</param>
        /// <param name="code">OnlineRefillSoftware unique code.</param>
        /// <param name="name">Name of OnlineRefillSoftware.</param>
        public OnlineFillSoftware(string id, string code, string name) : base(id, code, name, null) { }

        /// <summary>
        /// Initalizes a new isntance of the OnlineRefillSoftware object.
        /// </summary>
        /// <param name="code">OnlineRefillSoftware unique code.</param>
        /// <param name="name">Name of OnlineRefillSoftware.</param>
        public OnlineFillSoftware(string code, string name) : base(null, code, name, null) { }

        /// <summary>
        /// Initalizes a new isntance of the OnlineRefillSoftware object.
        /// </summary>
        /// <param name="id">OnlineRefillSoftware unique identifier.</param>
        /// <param name="code">OnlineRefillSoftware unique code.</param>
        /// <param name="name">Name of OnlineRefillSoftware.</param>
        /// <param name="description">Description of OnlineRefillSoftware.</param>
        public OnlineFillSoftware(string id, string code, string name, string description) :
            base(id, code, name, description) { }

        #endregion

        /// <summary>
        /// Returns a string that represents of the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}
