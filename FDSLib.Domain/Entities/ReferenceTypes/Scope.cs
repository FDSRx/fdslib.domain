﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities
{
    public class Scope : BaseReferenceType
    {

        /// <summary>
        /// Creates a new Scope object.
        /// </summary>
        public Scope() { }

        /// <summary>
        /// Creates a new Scope object.
        /// </summary>
        /// <param name="name">Name of Scope.</param>
        public Scope(string name) : base(null, null, name, null) { }

        /// <summary>
        /// Creates a new Scope object.
        /// </summary>
        /// <param name="id">Scope unique identifier.</param>
        /// <param name="code">Scope unique code.</param>
        /// <param name="name">Name of Scope.</param>
        public Scope(string id, string code, string name) : base(id, code, name, null) { }

        /// <summary>
        /// Creates a new Scope object.
        /// </summary>
        /// <param name="code">Scope unique code.</param>
        /// <param name="name">Name of Scope.</param>
        public Scope(string code, string name) : base(null, code, name, null) { }

        /// <summary>
        /// Creates a new Scope object.
        /// </summary>
        /// <param name="id">Scope unique identifier.</param>
        /// <param name="code">Scope unique code.</param>
        /// <param name="name">Name of Scope.</param>
        /// <param name="description">Description of Scope.</param>
        public Scope(string id, string code, string name, string description) :
            base(id, code, name, description) { }


        /// <summary>
        /// Returns the "Name" property of the Scope.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }

    }
}
