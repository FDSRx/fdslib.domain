﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities
{

    [DataContract]
    [KnownType(typeof(BaseReferenceType))]
    [KnownType(typeof(ReferenceTypeMetaData))]
    public class ColorItem : BaseReferenceType
    {

        /// <summary>
        /// The hex value of the color.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string HexValue { get; set; }

        /// <summary>
        /// The RGB value of the color.
        /// </summary>
        [DefaultValue(null)]
        [DataMember(EmitDefaultValue = false)]
        public string RGB { get; set; }

        #region Constructors

        /// <summary>
        /// Creates a new ColorItem object.
        /// </summary>
        public ColorItem() { }

        /// <summary>
        /// Creates a new ColorItem object.
        /// </summary>
        /// <param name="name">Name of ColorItem.</param>
        public ColorItem(string name) : base(null, null, name, null) { }

        /// <summary>
        /// Creates a new ColorItem object.
        /// </summary>
        /// <param name="id">ColorItem unique identifier.</param>
        /// <param name="code">ColorItem unique code.</param>
        /// <param name="name">Name of ColorItem.</param>
        public ColorItem(string id, string code, string name) : base(id, code, name, null) { }

        /// <summary>
        /// Creates a new ColorItem object.
        /// </summary>
        /// <param name="code">ColorItem unique code.</param>
        /// <param name="name">Name of ColorItem.</param>
        public ColorItem(string code, string name) : base(null, code, name, null) { }

        /// <summary>
        /// Creates a new ColorItem object.
        /// </summary>
        /// <param name="id">ColorItem unique identifier.</param>
        /// <param name="code">ColorItem unique code.</param>
        /// <param name="name">Name of ColorItem.</param>
        /// <param name="description">Description of ColorItem.</param>
        public ColorItem(string id, string code, string name, string description) :
            base(id, code, name, description) { }

        #endregion

        /// <summary>
        /// Returns the "Name" property of the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }


    }


}
