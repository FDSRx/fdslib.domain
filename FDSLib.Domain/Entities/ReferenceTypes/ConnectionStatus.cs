﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    [KnownType(typeof(BaseReferenceType))]
    [KnownType(typeof(ReferenceTypeMetaData))]
    public class ConnectionStatus : BaseReferenceType
    {
        /// <summary>
        /// Indicates whether the connection status is considered active.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Indicates if the connection is manually (self handled) or system maintained.
        /// </summary>
        public bool IsSelfOperated { get; set; }

        /// <summary>
        /// Creates a new ConnectionStatus object.
        /// </summary>
        public ConnectionStatus() { }

        /// <summary>
        /// Creates a new ConnectionStatus object.
        /// </summary>
        /// <param name="name">Name of ConnectionStatus.</param>
        public ConnectionStatus(string name) : base(null, null, name, null) {  }

        /// <summary>
        /// Creates a new ConnectionStatus object.
        /// </summary>
        /// <param name="id">ConnectionStatus unique identifier.</param>
        /// <param name="code">ConnectionStatus unique code.</param>
        /// <param name="name">Name of ConnectionStatus.</param>
        public ConnectionStatus(string id, string code, string name) : base(id, code, name, null) {  }

        /// <summary>
        /// Creates a new ConnectionStatus object.
        /// </summary>
        /// <param name="code">ConnectionStatus unique code.</param>
        /// <param name="name">Name of ConnectionStatus.</param>
        public ConnectionStatus(string code, string name) : base(null, code, name, null) {  }

        /// <summary>
        /// Creates a new ConnectionStatus object.
        /// </summary>
        /// <param name="id">ConnectionStatus unique identifier.</param>
        /// <param name="code">ConnectionStatus unique code.</param>
        /// <param name="name">Name of ConnectionStatus.</param>
        /// <param name="description">Description of ConnectionStatus.</param>
        public ConnectionStatus(string id, string code, string name, string description) :
            base(id, code, name, description) {  }

        /// <summary>
        /// Returns the name of the ConnectionStatus.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}
