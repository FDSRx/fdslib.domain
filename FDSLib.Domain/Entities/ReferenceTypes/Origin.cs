﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    [KnownType(typeof(BaseReferenceType))]
    [KnownType(typeof(ReferenceTypeMetaData))]
    public class Origin : BaseReferenceType
    {
        /// <summary>
        /// A unique data identifier that is a reference back to the origin of the data.
        /// </summary>
        public string DataKey { get; set; }

        /// <summary>
        /// Creates a new Gender object.
        /// </summary>
        public Origin() { }

        /// <summary>
        /// Creates a new Origin object.
        /// </summary>
        /// <param name="name">Name of Origin.</param>
        public Origin(string name) : base(null, null, name, null) { }

        /// <summary>
        /// Creates a new Gender object.
        /// </summary>
        /// <param name="id">Gender unique identifier.</param>
        /// <param name="code">Gender unique code.</param>
        /// <param name="name">Name of Gender.</param>
        public Origin(string id, string code, string name) : base(id, code, name, null) { }

        /// <summary>
        /// Creates a new Gender object.
        /// </summary>
        /// <param name="code">Gender unique code.</param>
        /// <param name="name">Name of Gender.</param>
        public Origin(string code, string name) : base(null, code, name, null) { }

        /// <summary>
        /// Creates a new Origin object.
        /// </summary>
        /// <param name="id">Origin unique identifier.</param>
        /// <param name="code">Origin unique code.</param>
        /// <param name="name">Name of Origin.</param>
        /// <param name="description">Description of Origin.</param>
        public Origin(string id, string code, string name, string description) :
            base(id, code, name, description) { }


        /// <summary>
        /// Returns the "Name" property of the Origin.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }

    }
}
