﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    [KnownType(typeof(BaseReferenceType))]
    [KnownType(typeof(ReferenceTypeMetaData))]
    public class Gender : BaseReferenceType
    {
        /// <summary>
        /// Creates a new Gender object.
        /// </summary>
        public Gender() { }

        /// <summary>
        /// Creates a new Gender object.
        /// </summary>
        /// <param name="name">Name of gender.</param>
        public Gender(string name) : base(null, null, name, null) { }

        /// <summary>
        /// Creates a new Gender object.
        /// </summary>
        /// <param name="id">Gender unique identifier.</param>
        /// <param name="code">Gender unique code.</param>
        /// <param name="name">Name of gender.</param>
        public Gender(string id, string code, string name) : base(id, code, name, null) { }

        /// <summary>
        /// Creates a new Gender object.
        /// </summary>
        /// <param name="code">Gender unique code.</param>
        /// <param name="name">Name of gender.</param>
        public Gender(string code, string name) : base(null, code, name, null) { }

        /// <summary>
        /// Creates a new Gender object.
        /// </summary>
        /// <param name="id">Gender unique identifier.</param>
        /// <param name="code">Gender unique code.</param>
        /// <param name="name">Name of gender.</param>
        /// <param name="description">Description of gender.</param>
        public Gender(string id, string code, string name, string description) :
            base(id, code, name, description) { }

        /// <summary>
        /// Returns the name of the gender.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}