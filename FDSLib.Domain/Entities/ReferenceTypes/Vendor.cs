﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities
{
    public class Vendor : BaseReferenceType
    {
        /// <summary>
        /// Creates a new Vendor object.
        /// </summary>
        public Vendor() { }

        /// <summary>
        /// Creates a new Vendor object.
        /// </summary>
        /// <param name="name">Name of Vendor.</param>
        public Vendor(string name) : base(null, null, name, null) { }

        /// <summary>
        /// Creates a new Vendor object.
        /// </summary>
        /// <param name="id">Vendor unique identifier.</param>
        /// <param name="code">Vendor unique code.</param>
        /// <param name="name">Name of Vendor.</param>
        public Vendor(string id, string code, string name) : base(id, code, name, null) { }

        /// <summary>
        /// Creates a new Vendor object.
        /// </summary>
        /// <param name="code">Vendor unique code.</param>
        /// <param name="name">Name of Vendor.</param>
        public Vendor(string code, string name) : base(null, code, name, null) { }

        /// <summary>
        /// Creates a new Vendor object.
        /// </summary>
        /// <param name="id">Vendor unique identifier.</param>
        /// <param name="code">Vendor unique code.</param>
        /// <param name="name">Name of Vendor.</param>
        /// <param name="description">Description of Vendor.</param>
        public Vendor(string id, string code, string name, string description) :
            base(id, code, name, description) { }


        /// <summary>
        /// Returns the "Name" property of the Vendor object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }

    }
}
