﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    [KnownType(typeof(BaseReferenceType))]
    [KnownType(typeof(ReferenceTypeMetaData))]
    public class Preference : BaseReferenceType
    {

        /// <summary>
        /// Creates a new Preference object.
        /// </summary>
        public Preference() { }

        /// <summary>
        /// Creates a new Preference object.
        /// </summary>
        /// <param name="name">Name of Language.</param>
        public Preference(string name) : base(null, null, name, null) { }

        /// <summary>
        /// Creates a new Preference object.
        /// </summary>
        /// <param name="id">Preference unique identifier.</param>
        /// <param name="code">Preference unique code.</param>
        /// <param name="name">Name of Preference.</param>
        public Preference(string id, string code, string name) : base(id, code, name, null) { }

        /// <summary>
        /// Creates a new Preference object.
        /// </summary>
        /// <param name="code">Preference unique code.</param>
        /// <param name="name">Name of Preference.</param>
        public Preference(string code, string name) : base(null, code, name, null) { }

        /// <summary>
        /// Creates a new Preference object.
        /// </summary>
        /// <param name="id">Preference unique identifier.</param>
        /// <param name="code">Preference unique code.</param>
        /// <param name="name">Name of Preference.</param>
        /// <param name="description">Description of Preference.</param>
        public Preference(string id, string code, string name, string description) :
            base(id, code, name, description) { }


        /// <summary>
        /// Returns the "Name" property of the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }

    }
}
