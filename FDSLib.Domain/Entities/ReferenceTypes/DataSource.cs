﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    [KnownType(typeof(BaseReferenceType))]
    [KnownType(typeof(ReferenceTypeMetaData))]
    public class DataSource : BaseReferenceType
    {

        #region Constructors


        /// <summary>
        /// Creates a new DataSource object.
        /// </summary>
        public DataSource() { }

        /// <summary>
        /// Creates a new DataSource object.
        /// </summary>
        /// <param name="name">Name of DataSource.</param>
        public DataSource(string name) : base(null, null, name, null) { }

        /// <summary>
        /// Creates a new DataSource object.
        /// </summary>
        /// <param name="id">DataSource unique identifier.</param>
        /// <param name="code">DataSource unique code.</param>
        /// <param name="name">Name of DataSource.</param>
        public DataSource(string id, string code, string name) : base(id, code, name, null) { }

        /// <summary>
        /// Creates a new DataSource object.
        /// </summary>
        /// <param name="code">DataSource unique code.</param>
        /// <param name="name">Name of DataSource.</param>
        public DataSource(string code, string name) : base(null, code, name, null) { }

        /// <summary>
        /// Creates a new DataSource object.
        /// </summary>
        /// <param name="id">DataSource unique identifier.</param>
        /// <param name="code">DataSource unique code.</param>
        /// <param name="name">Name of DataSource.</param>
        /// <param name="description">Description of DataSource.</param>
        public DataSource(string id, string code, string name, string description) :
            base(id, code, name, description) { }

        #endregion

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}
