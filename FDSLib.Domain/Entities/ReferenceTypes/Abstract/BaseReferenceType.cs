﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FDSLib.Domain.Entities
{
    /// <summary>
    /// Base reference type that constructs concrete reference objects.
    /// </summary>
    public abstract class BaseReferenceType : ReferenceTypeMetaData
    {

        #region Constructors

        /// <summary>
        /// Creates a new reference type object.
        /// </summary>
        protected internal BaseReferenceType() { }

        /// <summary>
        /// Creates a new reference type object.
        /// </summary>
        /// <param name="name">Name of reference type.</param>
        protected internal BaseReferenceType(string name) : this(null, null, name, null) { }

        /// <summary>
        /// Creates a new reference type object.
        /// </summary>
        /// <param name="id">Reference type unique identifier.</param>
        /// <param name="code">Reference type unqiue code.</param>
        /// <param name="name">Name of reference type.</param>
        protected internal BaseReferenceType(string id, string code, string name) : this(id, code, name, null) { }

        /// <summary>
        /// Creates a new reference type object.
        /// </summary>
        /// <param name="code">Reference type unqiue code.</param>
        /// <param name="name">Name of type.</param>
        protected internal BaseReferenceType(string code, string name) : this(null, code, name, null) { }

        /// <summary>
        /// Creates a new reference type object.
        /// </summary>
        /// <param name="id">Reference type unique identifier.</param>
        /// <param name="code">Reference type unqiue code.</param>
        /// <param name="name">Name of reference type.</param>
        /// <param name="description">Description of reference type.</param>
        protected internal BaseReferenceType(string id, string code, string name, string description)
        {
            if (String.IsNullOrEmpty(code))
            {
                throw new ArgumentNullException("code");
            }

            this.Id = id;
            this.Code = code;
            this.Name = name;
            this.Description = description;
            this.Guid = Guid.NewGuid();
        }

        #endregion

        ///<summary>
        /// Gets the reference type meta data.
        ///</summary>
        public IReferenceType GetMetaData()
        {
            return new ReferenceTypeMetaData(this);
        }

        /// <summary>
        /// Returns the name of the reference type.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }

    }
}