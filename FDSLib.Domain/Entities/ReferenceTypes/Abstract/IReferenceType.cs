﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FDSLib.Domain.Entities
{
    public interface IReferenceType : IEntity
    {

        /// <summary>
        /// Type unique code.
        /// </summary>
        string Code { get; set; }

        /// <summary>
        /// Name of type.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Description of type.
        /// </summary>
        string Description { get; set; }
    }
}