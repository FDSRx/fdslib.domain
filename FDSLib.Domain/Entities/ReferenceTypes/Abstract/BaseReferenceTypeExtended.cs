﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FDSLib.Domain.Entities
{
    public abstract class BaseReferenceTypeExtended : BaseReferenceType
    {
        /// <summary>
        /// A shortened name of the type (if applicable)
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// An abbreviation of the type.
        /// </summary>
        public string Abbreviation { get; set; }

        /// <summary>
        /// The order in which the element should appear in a list.
        /// </summary>
        public int SortOrder { get; set; }

        /// <summary>
        /// Creates a new reference type object.
        /// </summary>
        protected internal BaseReferenceTypeExtended() { }

        /// <summary>
        /// Creates a new reference type object.
        /// </summary>
        /// <param name="name">Name of reference type.</param>
        protected internal BaseReferenceTypeExtended(string name) : this(null, null, name, null) { }

        /// <summary>
        /// Creates a new reference type object.
        /// </summary>
        /// <param name="id">Reference type unique identifier.</param>
        /// <param name="code">Reference type unqiue code.</param>
        /// <param name="name">Name of reference type.</param>
        protected internal BaseReferenceTypeExtended(string id, string code, string name) : this(id, code, name, null) { }

        /// <summary>
        /// Creates a new reference type object.
        /// </summary>
        /// <param name="code">Reference type unqiue code.</param>
        /// <param name="name">Name of type.</param>
        protected internal BaseReferenceTypeExtended(string code, string name) : this(null, code, name, null) { }

        /// <summary>
        /// Creates a new reference type object.
        /// </summary>
        /// <param name="id">Reference type unique identifier.</param>
        /// <param name="code">Reference type unqiue code.</param>
        /// <param name="name">Name of reference type.</param>
        /// <param name="description">Description of reference type.</param>
        protected internal BaseReferenceTypeExtended(string id, string code, string name, string description)
        {
            if (String.IsNullOrEmpty(code))
            {
                throw new ArgumentNullException("code");
            }

            this.Id = id;
            this.Code = code;
            this.Name = name;
            this.Description = description;
            this.Guid = Guid.NewGuid();
        }

        /// <summary>
        /// Returns the name of the reference type.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }
    
    }
}