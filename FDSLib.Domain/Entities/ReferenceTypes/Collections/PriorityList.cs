﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace FDSLib.Domain.Entities
{
    [XmlRoot("Priorities")]
    public class PriorityList : List<Priority>
    {
    }
}
