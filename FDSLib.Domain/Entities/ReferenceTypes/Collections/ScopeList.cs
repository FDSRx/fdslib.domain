﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace FDSLib.Domain.Entities
{
    [XmlRoot("Scopes")]
    public class ScopeList : List<Scope>
    {
    }
}
