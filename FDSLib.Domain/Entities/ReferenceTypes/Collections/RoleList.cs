﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace FDSLib.Domain.Entities
{
    [XmlRoot(ElementName = "Roles")]
    [CollectionDataContract(Name = "Roles", ItemName = "Role")]
    public class RoleList : List<Role>
    {
    }
}
