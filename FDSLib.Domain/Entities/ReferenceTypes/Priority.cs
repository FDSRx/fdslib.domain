﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    [KnownType(typeof(BaseReferenceType))]
    [KnownType(typeof(ReferenceTypeMetaData))]
    public class Priority : BaseReferenceType
    {
        /// <summary>
        /// Creates a new Priority object.
        /// </summary>
        public  Priority() { }

        /// <summary>
        /// Creates a new Priority object.
        /// </summary>
        /// <param name="name">Name of Priority.</param>
        public Priority(string name) : base(null, null, name, null) { }

        /// <summary>
        /// Creates a new Priority object.
        /// </summary>
        /// <param name="id">Priority unique identifier.</param>
        /// <param name="code">Priority unique code.</param>
        /// <param name="name">Name of Priority.</param>
        public Priority(string id, string code, string name) : base(id, code, name, null) { }

        /// <summary>
        /// Creates a new Priority object.
        /// </summary>
        /// <param name="code">Priority unique code.</param>
        /// <param name="name">Name of Priority.</param>
        public Priority(string code, string name) : base(null, code, name, null) { }

        /// <summary>
        /// Creates a new Priority object.
        /// </summary>
        /// <param name="id">Priority unique identifier.</param>
        /// <param name="code">Priority unique code.</param>
        /// <param name="name">Name of Priority.</param>
        /// <param name="description">Description of Priority.</param>
        public Priority(string id, string code, string name, string description) :
            base(id, code, name, description) { }


        /// <summary>
        /// Returns the "Name" property of the Priority.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }

    }
}
