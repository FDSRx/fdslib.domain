﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FDSLib.Domain.Entities
{
    /// <summary>
    /// Object that represents the core meta data of a reference object.
    /// </summary>
    public class ReferenceTypeMetaData : Entity, IReferenceType
    {
        /// <summary>
        /// Type unique code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Name of type.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Description of type.
        /// </summary>
        public string Description { get; set; }

        #region Constructors

        /// <summary>
        /// Creates a new type meta data object.
        /// </summary>
        protected ReferenceTypeMetaData() { }

        /// <summary>
        /// Creates a new type meta data object.
        /// </summary>
        /// <param name="type">ILookupType object.</param>
        public ReferenceTypeMetaData(IReferenceType type)
        {
            this.Id = type.Id;
            this.Code = type.Code;
            this.Name = type.Name;
            this.Description = type.Description;
            this.Guid = type.Guid;
        }

        #endregion

        /// <summary>
        /// Returns the Name property of the object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}