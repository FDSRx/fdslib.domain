﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FDSLib.Domain.Entities
{
    public class ReferenceType : BaseReferenceType
    {

        #region Constructors

        /// <summary>
        /// Creates a new ReferenceType object.
        /// </summary>
        public ReferenceType() { }

        /// <summary>
        /// Creates a new ReferenceType object.
        /// </summary>
        /// <param name="name">Name of ReferenceType.</param>
        public ReferenceType(string name) : base(null, null, name, null) { }

        /// <summary>
        /// Creates a new ReferenceType object.
        /// </summary>
        /// <param name="id">ReferenceType unique identifier.</param>
        /// <param name="code">ReferenceType unique code.</param>
        /// <param name="name">Name of ReferenceType.</param>
        public ReferenceType(string id, string code, string name) : base(id, code, name, null) { }

        /// <summary>
        /// Creates a new ReferenceType object.
        /// </summary>
        /// <param name="code">ReferenceType unique code.</param>
        /// <param name="name">Name of ReferenceType.</param>
        public ReferenceType(string code, string name) : base(null, code, name, null) { }

        /// <summary>
        /// Creates a new ReferenceType object.
        /// </summary>
        /// <param name="id">ReferenceType unique identifier.</param>
        /// <param name="code">ReferenceType unique code.</param>
        /// <param name="name">Name of ReferenceType.</param>
        /// <param name="description">Description of ReferenceType.</param>
        public ReferenceType(string id, string code, string name, string description) :
            base(id, code, name, description) { }

        #endregion

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}
