﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    [KnownType(typeof(BaseReferenceType))]
    [KnownType(typeof(ReferenceTypeMetaData))]
    public class Application : BaseReferenceType
    {

        #region Constructors

        /// <summary>
        /// Creates a new Application object.
        /// </summary>
        public Application() { }

        /// <summary>
        /// Creates a new Application object.
        /// </summary>
        /// <param name="name">Name of Application.</param>
        public Application(string name) : base(null, null, name, null) { }

        /// <summary>
        /// Creates a new Application object.
        /// </summary>
        /// <param name="id">Application unique identifier.</param>
        /// <param name="code">Application unique code.</param>
        /// <param name="name">Name of Application.</param>
        public Application(string id, string code, string name) : base(id, code, name, null) { }

        /// <summary>
        /// Creates a new Application object.
        /// </summary>
        /// <param name="code">Application unique code.</param>
        /// <param name="name">Name of Application.</param>
        public Application(string code, string name) : base(null, code, name, null) { }

        /// <summary>
        /// Creates a new Application object.
        /// </summary>
        /// <param name="id">Application unique identifier.</param>
        /// <param name="code">Application unique code.</param>
        /// <param name="name">Name of Application.</param>
        /// <param name="description">Description of Application.</param>
        public Application(string id, string code, string name, string description) :
            base(id, code, name, description) { }


        #endregion

        /// <summary>
        /// Returns the "Name" property of the Application.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }

    }
}
