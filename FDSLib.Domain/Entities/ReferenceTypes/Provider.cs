﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    [KnownType(typeof(BaseReferenceType))]
    [KnownType(typeof(ReferenceTypeMetaData))]
    [Serializable]
    public class Provider : BaseReferenceType
    {

        #region Constructors

        /// <summary>
        /// Initalizes a new isntance of the Provider object.
        /// </summary>
        public Provider() { }

        /// <summary>
        /// Initalizes a new isntance of the Provider object.
        /// </summary>
        /// <param name="name">Name of Provider.</param>
        public Provider(string name) : base(null, null, name, null) { }

        /// <summary>
        /// Initalizes a new isntance of the Provider object.
        /// </summary>
        /// <param name="id">Provider unique identifier.</param>
        /// <param name="code">Provider unique code.</param>
        /// <param name="name">Name of Provider.</param>
        public Provider(string id, string code, string name) : base(id, code, name, null) { }

        /// <summary>
        /// Initalizes a new isntance of the Provider object.
        /// </summary>
        /// <param name="code">Provider unique code.</param>
        /// <param name="name">Name of Provider.</param>
        public Provider(string code, string name) : base(null, code, name, null) { }

        /// <summary>
        /// Initalizes a new isntance of the Provider object.
        /// </summary>
        /// <param name="id">Provider unique identifier.</param>
        /// <param name="code">Provider unique code.</param>
        /// <param name="name">Name of Provider.</param>
        /// <param name="description">Description of Provider.</param>
        public Provider(string id, string code, string name, string description) :
            base(id, code, name, description) { }

        #endregion

        /// <summary>
        /// Returns a string that represents of the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}
