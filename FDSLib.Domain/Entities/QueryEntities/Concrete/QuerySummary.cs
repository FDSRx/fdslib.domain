﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FDSLib.Domain.Entities
{
    [DataContract]
    [Serializable]
    public class QuerySummary
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the QuerySummary object.
        /// </summary>
        public QuerySummary()
        {
        }

        #endregion

        /// <summary>
        /// The total number of records.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public int TotalResults { get; set; }

        /// <summary>
        /// The total amount of time the query executed.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public decimal ExecutionTime { get; set; }

        /// <summary>
        /// Returns the System.String representation of the RecordCount property.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("{0}", this.TotalResults);
        }
    }
}
