﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Common.Extensions;

namespace FDSLib.Domain.Entities
{
    public static class PictureFileExtensions
    {
        /// <summary>
        /// Converts a domain PictureFile object to a PhotoImage object.
        /// </summary>
        /// <param name="pic">PictureFile object.</param>
        /// <returns></returns>
        public static PhotoImage ToPhotoImage(this PictureFile pic)
        {
            if (pic == null)
            {
                return new PhotoImage();
            }

            var model = new PhotoImage()
                        {
                            Id = pic.Id.ToStringOrNull(),
                            Name = pic.Name,
                            Caption = pic.Caption,
                            KeyName = pic.KeyName,
                            FileName = pic.FileName,
                            FileDataString = pic.FileDataString,
                            FileDataBinary = pic.FileDataBinary,
                            FilePath = pic.FilePath,
                            Description = pic.Description,
                            DateCreated = pic.DateCreated,
                            DateModified = pic.DateModified,
                            Language = pic.Language
                        };

            return model;
        }

        /// <summary>
        /// Converts an enumerable list of domain PictureFile objects to an enumerable list of PhotoImage objects.
        /// </summary>
        /// <param name="collection">List(Of PictureFile) object.</param>
        /// <returns></returns>
        public static List<PhotoImage> ToPhotoImageList(this List<PictureFile> collection)
        {
            var list = new List<PhotoImage>();

            if (collection == null)
            {
                return list;
            }

            foreach (var item in collection)
            {

                list.Add(item.ToPhotoImage());
            }

            return list;
        }



    }



}
