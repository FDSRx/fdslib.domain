﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FDSLib.Domain.Entities
{
    public static class ReferenceTypeExtensions
    {
        /// <summary>
        /// Returns the ReferenceTypeMetaData object.  If the object is null, it will return a new ReferenceTypeMetaData object.
        /// </summary>
        /// <param name="obj">ReferenceTypeMetaData object.</param>
        /// <returns></returns>
        public static ReferenceTypeMetaData GetOrNew(this ReferenceTypeMetaData obj)
        {
            if (obj == null)
            {
                return new ReferenceType();
            }

            return obj;
        }
    }
}
