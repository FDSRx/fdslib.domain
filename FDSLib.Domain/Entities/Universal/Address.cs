﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace FDSLib.Domain.Entities
{
    public class Address : Entity
    {
        /// <summary>
        /// The information identifying the exact address within the given Postal Code such as a street name
        /// and number or P.O. Box.
        /// </summary>
        [DefaultValue(null)]
        public string Line1 { get; set; }

        /// <summary>
        /// Supplemental address information such as Suite number, Apartment number, etc.  Used to further qualify a mailing address.
        /// </summary>
        [DefaultValue(null)]
        public string Line2 { get; set; }

        /// <summary>
        /// A specific incorporated municipality.  Commonly known as a city, town, or village.
        /// </summary>
        [DefaultValue(null)]
        public string City { get; set; }

        /// <summary>
        /// A territorial subdivision of a country having a central civil government or authority.  
        /// Commonly known as states, regions, or provinces.
        /// </summary>
        [DefaultValue(null)]
        public string State { get; set; }

        /// <summary>
        /// A code utilized to further qualify an address for the purpose of sorting and filtering mail.
        /// </summary>
        [DefaultValue(null)]
        public string PostalCode { get; set; }

        /// <summary>
        /// Initializes a new instance of the Address object.
        /// </summary>
        public Address()
        {
            //this.Line1 = String.Empty;
            //this.Line2 = String.Empty;
            //this.City = String.Empty;
            //this.State = String.Empty;
            //this.PostalCode = String.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the Address object.
        /// </summary>
        /// <param name="line1">The information identifying the exact address within the given Postal Code such as a street name
        /// and number or P.O. Box.</param>
        /// <param name="line2">Supplemental address information such as Suite number, Apartment number, etc.  Used to further qualify a mailing address.</param>
        /// <param name="city">A specific incorporated municipality.  Commonly known as a city, town, or village.</param>
        /// <param name="state">A territorial subdivision of a country having a central civil government or authority.  
        /// Commonly known as states, regions, or provinces.</param>
        /// <param name="postalCode">A code utilized to further qualify an address for the purpose of sorting and filtering mail.</param>
        public Address(string line1, string line2, string city, string state, string postalCode)
        {
            if (String.IsNullOrEmpty(line1))
            {
                throw new ArgumentNullException("line1");
            }

            if (String.IsNullOrEmpty(state))
            {
                throw new ArgumentNullException("state");
            }

            if (String.IsNullOrEmpty(postalCode))
            {
                throw new ArgumentNullException("postalCode");
            }

            this.Line1 = line1;
            this.Line2 = line2;
            this.City = city;
            this.State = state;
            this.PostalCode = postalCode;
        }

        /// <summary>
        /// Returns a formatted address.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("{0} {1} | {2}, {3} {4}", this.Line1, this.Line2, this.City, this.State, this.PostalCode);
        }
    }
}